package cn.zhangfusheng.elasticsearch.mybatis;

import cn.zhangfusheng.elasticsearch.exception.GlobalSystemException;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.session.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.Set;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 13:47:31
 */
public class ElasticSearchWithMybatisXmlConfig extends Configuration {

    public ElasticSearchWithMybatisXmlConfig(Set<String> scanPath) {
        try {
            for (String path : scanPath) {
                Resource[] resources = new PathMatchingResourcePatternResolver().getResources(path);
                for (Resource resource : resources) {
                    XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(
                            resource.getInputStream(), this, resource.toString(), this.getSqlFragments());
                    xmlMapperBuilder.parse();
                }
            }
        } catch (IOException e) {
            throw new GlobalSystemException(e);
        }
    }

}
