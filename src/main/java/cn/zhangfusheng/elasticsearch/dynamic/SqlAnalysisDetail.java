package cn.zhangfusheng.elasticsearch.dynamic;

import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.DslWithSql;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.condition.SqlCondition;
import cn.zhangfusheng.elasticsearch.constant.enumeration.DyanmicType;
import cn.zhangfusheng.elasticsearch.jexl.Jexl3Analysis;
import cn.zhangfusheng.elasticsearch.jexl.Jexl3Execute;
import cn.zhangfusheng.elasticsearch.jexl.analysis.Analysis;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author fusheng.zhang
 * @date 2022-03-10 11:26:32
 */
public class SqlAnalysisDetail {

    private final Map<String, Object> constantParams;

    private final Map<String, Integer> parameterNames;

    private final List<Analysis> analyses;

    private final List<SqlCondition> sqlConditions;

    private final Map<SqlCondition, List<Analysis>> conditionAnalysis;

    public SqlAnalysisDetail(
            Map<String, Object> constantParams, List<String> parameterNames,
            List<Analysis> analyses, DslWithSql dslWithSql) {
        this.constantParams = constantParams;
        this.analyses = analyses;
        this.sqlConditions = Arrays.asList(dslWithSql.conditions());
        this.conditionAnalysis =
                this.sqlConditions.stream().collect(Collectors.toMap(o -> o, o -> new Jexl3Analysis(o.value()).compile()));
        this.parameterNames = new HashMap<>(parameterNames.size());
        for (int i = 0; i < parameterNames.size(); i++) this.parameterNames.put(parameterNames.get(i), i);
    }

    @SuppressWarnings("DuplicatedCode")
    public DyanmicExecuteResult execute(Object[] args) {
        Map<String, Object> params = new HashMap<>(parameterNames.size());
        this.parameterNames.forEach((k, v) -> params.put(k, args[v]));
        StringBuilder result = new StringBuilder();
        List<Object> resultParams = new ArrayList<>(args.length);
        Jexl3Execute jexl3Execute = new Jexl3Execute(params, constantParams);
        this.analyses.forEach(analysis -> analysis.execute(jexl3Execute));
        result.append(jexl3Execute.getResult());
        resultParams.addAll(jexl3Execute.getArgs());

        StringBuilder conditionResult = new StringBuilder();
        boolean appednWhere = true;
        for (SqlCondition sqlCondition : sqlConditions) {
            Jexl3Execute jexl3Execute_ = new Jexl3Execute(params, constantParams);
            this.conditionAnalysis.get(sqlCondition).forEach(d -> d.execute(jexl3Execute_));
            String result_ = jexl3Execute_.getResult();
            if (StringUtils.isNotBlank(result_)) {
                if (appednWhere && StringUtils.isNotBlank(sqlCondition.splicing()) && sqlCondition.appendWhere()) {
                    conditionResult.append(" where ").append(result_.trim());
                    appednWhere = false;
                } else {
                    conditionResult.append(" ").append(sqlCondition.splicing()).append(" ");
                    conditionResult.append(result_.trim());
                }
                resultParams.addAll(jexl3Execute_.getArgs());
            }
        }
        result.append(conditionResult);
        return new DyanmicExecuteResult(DyanmicType.SQL, result, resultParams);
    }
}
