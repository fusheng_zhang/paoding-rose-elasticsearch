package cn.zhangfusheng.elasticsearch.dynamic.sql.es;

import com.google.common.collect.ImmutableList;
import io.github.iamazy.elasticsearch.dsl.antlr4.ElasticsearchParser.SqlContext;
import io.github.iamazy.elasticsearch.dsl.antlr4.Walker;
import io.github.iamazy.elasticsearch.dsl.sql.exception.ElasticSql2DslException;
import io.github.iamazy.elasticsearch.dsl.sql.model.ElasticDslContext;
import io.github.iamazy.elasticsearch.dsl.sql.model.ElasticSqlParseResult;
import io.github.iamazy.elasticsearch.dsl.sql.parser.QueryFromParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.QueryLimitParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.QueryParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.QueryRoutingsParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.QuerySelectFieldsParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.QueryWhereConditionParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.aggs.AggregateByQueryParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.delete.DeleteQueryParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.desc.QueryDescParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.insert.InsertQueryParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.query.score.DisMaxQueryParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.reindex.ReindexQueryParser;
import io.github.iamazy.elasticsearch.dsl.sql.parser.update.UpdateQueryParser;

import java.util.List;

/**
 * 将 sql 语句转换成 es 查询的 语法
 * 替代: io.github.iamazy.elasticsearch.dsl.sql.ElasticSql2DslParser 使用自定义的QueryOrderByParser
 * @author fusheng.zhang
 * @date 2022-02-23 16:36:09
 */
public class ElasticSql2DslParser {

    public ElasticSqlParseResult parse(String sql) throws ElasticSql2DslException {
        Walker walker = new Walker(sql);
        SqlContext sqlContext = walker.buildAntlrTree();
        ElasticDslContext elasticDslContext = new ElasticDslContext(sqlContext);
        buildSqlParserChain().forEach(parser -> parser.parse(elasticDslContext));
        return elasticDslContext.getParseResult();
    }

    private List<QueryParser> buildSqlParserChain() {
        return ImmutableList.of(
                new QuerySelectFieldsParser(),
                new QueryFromParser(),
                new QueryWhereConditionParser(),
                new DisMaxQueryParser(),
                new QueryRoutingsParser(),
                new AggregateByQueryParser(),
                new QueryOrderByParser(),
                new QueryLimitParser(),

                new QueryDescParser(),
                new ReindexQueryParser(),
                new InsertQueryParser(),
                new UpdateQueryParser(),
                new DeleteQueryParser());
    }
}
