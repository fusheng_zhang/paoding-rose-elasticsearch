package cn.zhangfusheng.elasticsearch.annotation;

import cn.zhangfusheng.elasticsearch.constant.ElasticSearchConstant;
import org.elasticsearch.action.support.WriteRequest;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * es 的一些配置<br/>
 * 只有遇到的第一个属性不为空的值生效
 * @author fusheng.zhang
 * @date 2022-04-20 12:45:34
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ElasticSearchConfig {

    /**
     * 是否开启事物<br/>
     * 实际是将操作转换为批量处理
     * @return
     */
    boolean openTransactional() default false;

    /**
     * 事物回滚的异常
     * @return
     */
    Class<? extends Throwable> rollbackFor() default Throwable.class;

    /**
     * 策略配置
     * @return WriteRequest.RefreshPolicy.WAIT_UNTIL: 使请求的内容对搜索可见为止,此刷新策略与高索引和搜索吞吐量兼容，但它会导致请求等待答复，直到刷新发生
     * @return WriteRequest.RefreshPolicy.NONE: 此请求后不刷新。默认设置。
     * @return WriteRequest.RefreshPolicy.IMMEDIATE: 强制刷新作为此请求的一部分.此刷新策略不会针对高索引或搜索吞吐量进行扩展，但有助于为流量非常低的索引提供一致的视图。
     */
    WriteRequest.RefreshPolicy refreshPolicy() default WriteRequest.RefreshPolicy.NONE;

    /**
     * 是否查询总命中数量
     * @return
     */
    boolean trackTotalHits() default false;

    /**
     * 启用滚动查询时,每次滚动查询的数据量大小<br/>
     * 必须小于{@link ElasticSearchConstant#MAX_DOC_SIZE}<br/>
     * 默认为最大值的一半5000
     * @return
     */
    int scrollSize() default 5000;

    /**
     * 滚动查询scrollId默认缓存时长<br/>
     * 单位毫秒
     * @return
     */
    long keepAlive() default 5000L;

    /**
     * requestOptions
     * @return
     */
    String requestOptions() default "default";
}
