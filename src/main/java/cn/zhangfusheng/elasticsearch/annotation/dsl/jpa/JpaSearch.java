package cn.zhangfusheng.elasticsearch.annotation.dsl.jpa;

import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段对应的查询关键字
 * @author fusheng.zhang
 * @date 2022-05-26 15:13:13
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface JpaSearch {

    /**
     * 字段名
     * @return
     */
    String value();

    /**
     * 查询关键字
     * @return
     */
    SearchKeyword searchKeyword();
}
