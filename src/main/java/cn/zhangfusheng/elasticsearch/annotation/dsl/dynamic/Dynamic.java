package cn.zhangfusheng.elasticsearch.annotation.dsl.dynamic;

import cn.zhangfusheng.elasticsearch.annotation.dsl.dynamic.condition.DynamicCondition;
import cn.zhangfusheng.elasticsearch.constant.enumeration.DyanmicType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 动态字符串语法
 * @author fusheng.zhang
 * @date 2022-06-20 14:53:30
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Dynamic {

    /**
     * 句式
     * @return
     */
    String value();

    /**
     * 字符串拼接<br/>
     * 如果 动态句式 内存在成立的条件,则使用该字符串拼接
     * @return
     */
    String splicing() default "";

    /**
     * 动态句式
     * @return
     */
    DynamicCondition[] conditions() default {};

    /**
     * 动态语句类型
     * @return
     */
    DyanmicType type() default DyanmicType.DSL;
}
