package cn.zhangfusheng.elasticsearch.annotation.dsl.sql.condition;

import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.DslWithSql;

/**
 * sql 条件拼接
 * @author fusheng.zhang
 * @date 2022-03-10 11:09:10
 */
public @interface SqlCondition {

    /**
     * 条件句式
     * 句式成立,采用 {splicing} 进行拼接
     * @return
     */
    String value();

    /**
     * 字符串拼接
     * and
     * or
     * list
     * order by
     * ...
     * @return
     */
    String splicing() default "and";

    /**
     * 是否追加 where
     * appendWhere=true,当该条件成立时,该条件前是否可以拼接 where 关键字
     * 如果: {@link DslWithSql#value()} 内存在 where 关键字.该字段必须为 false
     * 如果: splicing where order by/limit 等,该字段必须为false
     * @return
     */
    boolean appendWhere() default true;
}
