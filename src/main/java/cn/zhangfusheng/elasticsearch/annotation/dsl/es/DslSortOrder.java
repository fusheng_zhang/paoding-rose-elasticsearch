package cn.zhangfusheng.elasticsearch.annotation.dsl.es;

import cn.zhangfusheng.elasticsearch.function.DslSortOrderFunction;
import org.elasticsearch.search.sort.SortOrder;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 排序
 * @author fusheng.zhang
 * @date 2022-04-29 16:53:54
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DslSortOrder {

    /**
     * 排序方式
     * @return
     */
    SortOrder value() default SortOrder.ASC;

    /**
     * 对应查询的字段名
     * @return
     */
    String fieldName() default "";

    /**
     * 根据字段值,控制改字段是否排序的函数处理
     * @return
     */
    Class<? extends DslSortOrderFunction> function() default DslSortOrderFunction.class;
}
