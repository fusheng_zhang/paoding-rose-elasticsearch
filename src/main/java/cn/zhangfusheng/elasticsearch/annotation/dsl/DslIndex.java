package cn.zhangfusheng.elasticsearch.annotation.dsl;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 指定查询的索引类<br/>
 * 可以放置在方法上和类上<br/>
 * 默认提供的查询方法,该注解必须添加到查询参数对应的类上<br/>
 * mybatis 和 jpa,动态sql, 的查询方法,注解必须添加到方法上<br/>
 * 即: <br/>
 * 默认查询:通过参数获取要查询的索引和返回值
 * jpa,mybatis,动态sql:通过方法上的注释获取索引和返回值
 * @author fusheng.zhang
 * @date 2022-05-14 11:11:23
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DslIndex {

    /**
     * 查询指定索引对应的class<br/>
     * 如果为空,则使用对应的 Repository的index
     * @return
     */
    Class<?>[] value() default Void.class;

    /**
     * 返回值类型<br/>
     * List.Optional 必须指定
     * @return
     */
    Class<?> returnType() default Void.class;
}
