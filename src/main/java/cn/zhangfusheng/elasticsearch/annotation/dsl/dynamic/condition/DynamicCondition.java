package cn.zhangfusheng.elasticsearch.annotation.dsl.dynamic.condition;

/**
 * @author fusheng.zhang
 * @date 2022-06-20 14:56:07
 */
public @interface DynamicCondition {

    /**
     * 条件句式
     * 句式成立,采用 {splicing} 进行拼接
     * @return
     */
    String value();

    /**
     * 字符串拼接<br/>
     * 同上一句字符串拼接的字符串
     * @return
     */
    String splicing() default "";

    /**
     * 是否动态添加 Dynamic.splicing
     * @return
     */
    boolean appendDynamicSplicing() default true;
}
