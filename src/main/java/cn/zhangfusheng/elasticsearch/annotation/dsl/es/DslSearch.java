package cn.zhangfusheng.elasticsearch.annotation.dsl.es;

import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 将携带该注解的字段解析成Java es 的查询代码
 * @author fusheng.zhang
 * @date 2022-04-25 10:38:32
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DslSearch {

    /**
     * 对应查询的字段名
     * @return
     */
    String value() default "";

    /**
     * routing 标识
     * @return
     */
    boolean routing() default false;

    /**
     * 查询类型
     * @return
     */
    SearchType searchType() default SearchType.MUST;

    /**
     * 关键字
     * @return
     */
    SearchKeyword keyword() default SearchKeyword.TERM;
}
