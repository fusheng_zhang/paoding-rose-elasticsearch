package cn.zhangfusheng.elasticsearch.annotation.dsl.mybatis;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <pre>
 *     通过mybatis解析 xml 文件生成 dsl 语句
 *     支持 mybatis xml 的 标签语法
 *     如果Repository接口的方法上添加该注解,标注对应的 xml 的 nameSpace 和id 找到对应的 dsl语句
 * </pre>
 * @author fusheng.zhang
 * @date 2022-02-24 14:46:36
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DslWithMybatis {

    /**
     * name space
     * @return
     */
    String nameSpace();

    /**
     * id
     * @return
     */
    String id();
}
