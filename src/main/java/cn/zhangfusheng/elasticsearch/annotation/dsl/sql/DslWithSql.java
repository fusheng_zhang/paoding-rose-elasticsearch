package cn.zhangfusheng.elasticsearch.annotation.dsl.sql;

import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.condition.SqlCondition;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 动态sql 转换成
 * @author fusheng.zhang
 * @date 2022-03-08 14:31:45
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DslWithSql {

    /**
     * 句式
     * @return
     */
    String value();

    /**
     * 条件句式
     * @return
     */
    SqlCondition[] conditions() default {};
}
