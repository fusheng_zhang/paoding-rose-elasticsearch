package cn.zhangfusheng.elasticsearch.annotation.document;

import cn.zhangfusheng.elasticsearch.constant.enumeration.TransferType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据迁移注解
 * 从源索引迁移至当前索引
 * @author fusheng.zhang
 * @date 2022-02-24 15:02:25
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface IndexTransfer {

    /**
     * 源索引
     * @return
     */
    String sourceIndex();

    /**
     * 数据迁移方式
     * @return
     */
    TransferType type() default TransferType.DEFAULT;

    /**
     * 数据迁移编号
     * @return
     */
    String upgradeVersion();
}
