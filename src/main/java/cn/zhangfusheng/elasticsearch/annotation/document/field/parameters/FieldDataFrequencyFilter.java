package cn.zhangfusheng.elasticsearch.annotation.document.field.parameters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/fielddata.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/fielddata.html</a>
 * @author fusheng.zhang
 * @date 2022-05-06 20:32:29
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldDataFrequencyFilter {

    double min();

    double max();

    double min_segment_size();
}
