package cn.zhangfusheng.elasticsearch.annotation.document.field.parameters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author fusheng.zhang
 * @date 2022-05-06 20:31:06
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldData {

    boolean fielddata();

    FieldDataFrequencyFilter[] fielddata_frequency_filter();
}
