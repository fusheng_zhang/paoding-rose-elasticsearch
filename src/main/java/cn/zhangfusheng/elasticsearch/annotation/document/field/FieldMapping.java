package cn.zhangfusheng.elasticsearch.annotation.document.field;

import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.Alias;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.GeoPoint;
import cn.zhangfusheng.elasticsearch.constant.enumeration.FieldType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段映射配置
 * @author fusheng.zhang
 * @date 2022-02-24 15:15:35
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface FieldMapping {

    /**
     * 主键
     * @return
     */
    boolean primaryId() default false;

    /**
     * routing
     * @return
     */
    boolean routing() default false;

    /**
     * 忽略字段
     * @return
     */
    boolean ignore() default false;

    /**
     * 字段类型
     * @return
     */
    FieldType type() default FieldType.Default;

    /**
     * mapping parameters<br/>
     * 最多只能配置1个
     * @return
     */
    MappingParameters mappingParameters() default @MappingParameters;

    /**
     * geo point 属性补充
     * @return
     */
    GeoPoint geoPoint() default @GeoPoint;

    /**
     * alias 类型的补充
     * @return
     */
    Alias alias() default @Alias(path = "");

    /**
     * 标注字段为创建时间<br/>
     * 调用创建接口时,如果存在字段标注,则调整字段时间为当前时间
     * @return
     */
    boolean isCreateTime() default false;

    /**
     * 标注字段为更新时间<br/>
     * 调用更新接口时,如果存在字段标注,则调整字段时间为当前时间
     * @return
     */
    boolean isUpdateTime() default false;
}
