package cn.zhangfusheng.elasticsearch.annotation.document.field.parameters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * geo point 字段属性补充
 * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/geo-point.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/geo-point.html</a>
 * @author fusheng.zhang
 * @date 2022-05-10 19:45:15
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GeoPoint {

    /**
     * 如果true（默认）三个维度点将被接受（存储在源中）但只有纬度和经度值将被索引；第三维度被忽略。<br/>
     * 如果false，包含任何超过纬度和经度（二维）值的地理点会引发异常并拒绝整个文档。<br/>
     * script请注意，如果使用参数，则 无法设置。
     * @return
     */
    long ignore_z_value() default -1L;

    /**
     * 如果设置了此参数，则该字段将索引此脚本生成的值，而不是直接从源读取值。<br/>
     * 如果在输入文档上为此字段设置了值，则该文档将被拒绝并出现错误。<br/>
     * 脚本与它们的 运行时等效的格式相同，并且应该将点作为一对 (lat, lon) double 值发出。
     * @return
     */
    String script() default "";
}
