package cn.zhangfusheng.elasticsearch.annotation.document.field;

import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.FieldData;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.IndexPrefixes;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * long 类型的属性代替 boolean 类型的属性,-1L:默认值,不处理 0L:false 1L:true<br/>
 * 注意: long 类型的属性必须使用大写字母'L'进行标记,否则按false处理
 * mapping patameters<br/>
 * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-params.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-params.html</a>
 * @author fusheng.zhang
 * @date 2022-05-03 15:41:04
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MappingParameters {

    /**
     * 字符串字段的值analyzed通过 分析器传递，以将字符串转换为标记或 术语流<br/>
     * standard/ fingerprint/ keyword/ simple/ stop/ whitespace/
     * @return
     */
    String analyzer() default "";

    /**
     * true:强制尝试清理脏值以适应字段的数据类型
     * @return
     */
    long coerce() default -1L;

    /**
     * 该copy_to参数允许您将多个字段的值复制到一个组字段中，然后可以将其作为单个字段进行查询
     * @return
     */
    String[] copy_to() default {};

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/doc-values.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/doc-values.html</a>
     * @return
     */
    long doc_values() default -1L;

    /**
     * Elasticsearch 会尝试为您提供的所有字段编制索引，但有时您只想存储该字段而不对其编制索引<br/>
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/enabled.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/enabled.html</a>
     * @return
     */
    long enabled() default -1L;

    /**
     * 加载全局序数<br/>
     * 必须先构建全局序数映射，然后才能在搜索期间使用序数。默认情况下，映射在第一次需要全局序号时在搜索期间加载<br/>
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/eager-global-ordinals.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/eager-global-ordinals.html</a>
     * @return
     */
    long eager_global_ordinals() default -1L;

    /**
     * 最多只能配置1个<br/>
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/fielddata.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/fielddata.html</a>
     * TODO fielddata_frequency_filter
     * @return
     */
    FieldData[] fielddata() default {};

    /**
     * 时间格式化配置
     * @return
     */
    String format() default "";

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/ignore-above.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/ignore-above.html</a>
     * @return
     */
    int ignore_above() default -1;

    /**
     * 如果true，则忽略格式错误的地理点。如果false（默认），格式错误的地理点会引发异常并拒绝整个文档。<br/>
     * script如果地理点的纬度超出范围 -90 ⇐ 纬度 ⇐ 90，或者其经度超出范围 -180 ⇐ 经度 ⇐ 180，则认为地理点格式错误。<br/>
     * 请注意，如果使用该参数，则 无法设置。
     * @return
     */
    long ignore_malformed() default -1L;

    /**
     * 该字段应该是可搜索的吗？接受true（默认）和false.
     * @return
     */
    long index() default -1L;

    /**
     * doc,freqs,positions,offsets
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-options.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-options.html</a>
     * @return
     */
    String index_options() default "";

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-phrases.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-phrases.html</a>
     * @return
     */
    long index_phrases() default -1L;

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-prefixes.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-prefixes.html</a>
     * @return
     */
    IndexPrefixes[] index_prefixes() default {};

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/multi-fields.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/multi-fields.html</a>
     * @return
     */
    Class<?> fields() default Void.class;

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/norms.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/norms.html</a>
     * @return
     */
    long norms() default -1L;

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/null-value.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/null-value.html</a>
     * @return
     */
    String null_value() default "";

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/position-increment-gap.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/position-increment-gap.html</a>
     * @return
     */
    int position_increment_gap() default -1;

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/properties.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/properties.html</a>
     * @return
     */
    Class<?> properties() default Void.class;

    /**
     * BM25.classic.boolean
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/similarity.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/similarity.html</a>
     * @return
     */
    String similarity() default "";

    /**
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-store.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-store.html</a>
     * @return
     */
    long store() default -1L;

    /**
     * no.yes.with_positions.with_offsets.with_positions_offsets.with_positions_payloads.with_positions_offsets_payloads
     * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/term-vector.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/term-vector.html</a>
     * @return
     */
    String term_vector() default "";

    /**
     * 如果true（默认）三个维度点将被接受（存储在源中）但只有纬度和经度值将被索引；第三维度被忽略。<br/>
     * 如果false，包含任何超过纬度和经度（二维）值的地理点会引发异常并拒绝整个文档。<br/>
     * script请注意，如果使用参数，则 无法设置。
     * @return
     */
    long ignore_z_value() default -1L;

    /**
     * 如果设置了此参数，则该字段将索引此脚本生成的值，而不是直接从源读取值。<br/>
     * 如果在输入文档上为此字段设置了值，则该文档将被拒绝并出现错误。<br/>
     * 脚本与它们的 运行时等效的格式相同，并且应该将点作为一对 (lat, lon) double 值发出。
     * @return
     */
    String script() default "";

    /*
    normalizer
    boost

    copy_to
    doc_values
    dynamic
    enabled
    fielddata
    eager_global_ordinals
    ignore_above
    ignore_malformed
    index_options
    index_phrases
    index_prefixes
    index
    fields
    norms
    null_value
    position_increment_gap
    properties
    search_analyzer
    similarity
    store
    term_vector
     */
}
