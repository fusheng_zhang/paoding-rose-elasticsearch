package cn.zhangfusheng.elasticsearch.annotation.document.field.parameters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 字段类型alias的补充
 * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/alias.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/alias.html</a>
 * @author fusheng.zhang
 * @date 2022-05-11 19:09:35
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Alias {

    String path();
}
