package cn.zhangfusheng.elasticsearch.annotation.document.field.parameters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-prefixes.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/index-prefixes.html</a>
 * @author fusheng.zhang
 * @date 2022-05-06 20:15:02
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IndexPrefixes {

    /**
     * 索引的最小前缀长度。必须大于 0，默认为 2。该值包括在内。
     * @return
     */
    int min_chars() default 2;

    /**
     * 要索引的最大前缀长度。必须小于 20，默认为 5。该值包括在内。
     * @return
     */
    int max_chars() default 5;
}
