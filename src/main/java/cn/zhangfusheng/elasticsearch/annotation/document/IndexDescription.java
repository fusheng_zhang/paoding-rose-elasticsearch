package cn.zhangfusheng.elasticsearch.annotation.document;

import cn.zhangfusheng.elasticsearch.constant.enumeration.TransferType;
import cn.zhangfusheng.elasticsearch.cycle.CreateMappingBefore;
import cn.zhangfusheng.elasticsearch.cycle.CreateMappingEnd;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 索引 版本控制
 * @author fusheng.zhang
 * @date 2022-02-24 14:57:33
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface IndexDescription {

    /**
     * 索引名称
     * 默认使用类名的小驼峰
     * @return
     */
    String value() default "";

    /**
     * 索引别名
     * @return
     */
    String alias() default "";

    /**
     * 索引版本号
     * @return
     */
    int version();

    /**
     * 索引升级编号
     * @return
     */
    String upgradeVersion();

    /**
     * 创建 mapping 前执行
     * @return
     */
    Class<? extends CreateMappingBefore>[] createMappingBefore() default {};

    /**
     * 创建mapping后执行
     * @return
     */
    Class<? extends CreateMappingEnd>[] createMappingEnd() default {};

    /**
     * 数据迁移方式
     * @return
     */
    TransferType transferType() default TransferType.DEFAULT;
}
