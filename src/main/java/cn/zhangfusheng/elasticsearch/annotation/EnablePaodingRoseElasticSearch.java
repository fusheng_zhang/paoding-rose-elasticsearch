package cn.zhangfusheng.elasticsearch.annotation;

import cn.zhangfusheng.elasticsearch.register.PaodingRoseElasticSearchAnnotationRegister;
import cn.zhangfusheng.elasticsearch.register.PaodingRoseElasticSearchRegister;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 开启 paoding rose ElasticSearch
 * @author fusheng.zhang
 * @date 2022-02-24 10:51:56
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({PaodingRoseElasticSearchRegister.class, PaodingRoseElasticSearchAnnotationRegister.class})
public @interface EnablePaodingRoseElasticSearch {


    @AliasFor("scanReposityPackages")
    String[] value() default {};

    /**
     * 要扫描的 {@link cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository} 的实现类的包
     * @return
     */
    String[] scanReposityPackages() default {};

    /**
     * mybatis xml 的路径
     * @return
     */
    String[] mybatisXmlScanPath() default {"classpath*:mapper/**/*.xml"};

    /**
     * 文档setting 配置的路径
     * @return
     */
    String[] settingJsonScanPath() default {"classpath*:setting/**/*.json"};

    /**
     * Repository 的代理方式
     * @return
     */
    AdviceMode mode() default AdviceMode.ASPECTJ;
}
