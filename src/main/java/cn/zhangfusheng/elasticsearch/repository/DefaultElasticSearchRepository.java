package cn.zhangfusheng.elasticsearch.repository;

import cn.zhangfusheng.elasticsearch.scan.ElasticSearchEntityRepositoryDetail;
import cn.zhangfusheng.elasticsearch.template.ElasticSearchRestTemplate;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 15:11:14
 */
public class DefaultElasticSearchRepository<T>
        implements DefaultRepository<T>, DefaultElasticSearchEditRepository<T>, DefaultElasticSearchQueryRepository<T> {

    private final ElasticSearchRestTemplate elasticSearchRestTemplate;
    private final ElasticSearchEntityRepositoryDetail entityRepositoryDetail;

    public DefaultElasticSearchRepository(
            ElasticSearchRestTemplate elasticSearchRestTemplate, ElasticSearchEntityRepositoryDetail entityRepositoryDetail) {
        this.elasticSearchRestTemplate = elasticSearchRestTemplate;
        this.entityRepositoryDetail = entityRepositoryDetail;
    }

    @Override
    public ElasticSearchRestTemplate elasticSearchRestTemplate() {
        return elasticSearchRestTemplate;
    }

    @Override
    public ElasticSearchEntityRepositoryDetail entityRepositoryDetail() {
        return entityRepositoryDetail;
    }
}
