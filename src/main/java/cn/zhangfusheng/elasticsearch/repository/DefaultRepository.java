package cn.zhangfusheng.elasticsearch.repository;

import cn.zhangfusheng.elasticsearch.scan.ElasticSearchEntityRepositoryDetail;
import cn.zhangfusheng.elasticsearch.template.ElasticSearchRestTemplate;

/**
 * @author fusheng.zhang
 * @date 2022-03-01 11:01:18
 */
public interface DefaultRepository<T> extends ElasticSearchRepository<T> {

    ElasticSearchRestTemplate elasticSearchRestTemplate();

    ElasticSearchEntityRepositoryDetail entityRepositoryDetail();
}
