package cn.zhangfusheng.elasticsearch.repository;

import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;

import java.util.List;
import java.util.Optional;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 13:39:27
 */
public interface ElasticSearchRepository<T> {

    /**
     * 新建一条数据
     * @param t
     * @return
     */
    boolean insert(T t);

    /**
     * 根据id更新一条数据
     * @param t           要更新的试题数据
     * @param excludeNull 是否排除为null 的字段
     *                    可以实现全字段数据更新,或者某些字段更新
     *                    true: 如果 某个字段值为null,不会修改该字段值
     *                    false:如果 某个字段值为null,则会将该字段更新为null
     * @return
     */
    boolean updateById(T t, boolean excludeNull);

    /**
     * 根据主键删除
     * @param primaryId
     * @return
     */
    boolean deleteById(String primaryId);

    /**
     * 根据主键删除
     * @param primaryId 主键
     * @param routing   routing
     * @return
     */
    boolean deleteById(String primaryId, String routing);

    /**
     * 根据主键查询
     * @param primaryId
     * @return
     */
    Optional<T> findById(String primaryId);

    /**
     * 根据主键查询
     * @param primaryId 主键值
     * @param routing   routing
     * @return
     */
    Optional<T> findById(String primaryId, String routing);

    /**
     * 根据查询条件只存在一条返回值<br/>
     * @param q DslSearch:配置查询条件 DslSortOrder:排序配置
     * @return
     */
    <Q, R> Optional<R> findOne(Q q);

    /**
     * 查询全部符合条件的数据
     * @param q DslSearch:配置查询条件 DslSortOrder:排序配置
     * @return
     */
    <Q, R> List<R> findAll(Q q);

    /**
     * 分页查询<br/>
     * 1.利用 from size 可以查询到1000条数据的分页,可以跳页查询<br/>
     * 2.超过1000条后的数据,必须携带参数searchAfter,利用search_after进行分页查询,不在可以跳页查询
     * @param q           DslSearch:配置查询条件 DslSortOrder:排序配置
     * @param pageRequest 分页查询的参数
     * @return
     */
    <Q, R> PageResponse<R> findForPage(Q q, PageRequest pageRequest);
}
