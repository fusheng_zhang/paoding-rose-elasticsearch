package cn.zhangfusheng.elasticsearch.exception;

import org.springframework.core.NestedRuntimeException;

/**
 * @author fusheng.zhang
 * @date 2022-05-02 12:06:26
 */
public class InitRepositoryException extends NestedRuntimeException {

    public InitRepositoryException(Throwable cause) {
        super(cause.getMessage(), cause);
        this.setStackTrace(cause.getStackTrace());
    }
}
