package cn.zhangfusheng.elasticsearch.exception;

import org.slf4j.helpers.MessageFormatter;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 13:57:47
 */
@SuppressWarnings("LombokSetterMayBeUsed")
public class GlobalSystemException extends RuntimeException {

    private String message;

    public GlobalSystemException(String msg) {
        this.message = msg;
    }

    public GlobalSystemException(String message, Object... objects) {
        this.message = MessageFormatter.arrayFormat(message, objects).getMessage();
    }

    public GlobalSystemException(Throwable throwable) {
        this.message = throwable.getMessage();
        this.setStackTrace(throwable.getStackTrace());
    }

    public GlobalSystemException(String message, Throwable throwable) {
        this.message = message;
        this.setStackTrace(throwable.getStackTrace());
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}