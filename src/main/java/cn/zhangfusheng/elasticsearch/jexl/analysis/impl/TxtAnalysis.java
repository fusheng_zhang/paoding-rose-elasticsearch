package cn.zhangfusheng.elasticsearch.jexl.analysis.impl;

import cn.zhangfusheng.elasticsearch.jexl.Jexl3Execute;
import cn.zhangfusheng.elasticsearch.jexl.analysis.Analysis;
import lombok.Data;

import java.util.Map;

/**
 * 普通文本 表达式
 * @author fusheng.zhang
 * @date 2022-03-05 16:24:15
 */
@Data
public class TxtAnalysis implements Analysis {

    /**
     * 普通文本
     */
    private final String txt;

    public TxtAnalysis(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        return this.txt;
    }

    @Override
    public void execute(Jexl3Execute jexl3Execute) {
        jexl3Execute.setStr(this.txt);
    }
}
