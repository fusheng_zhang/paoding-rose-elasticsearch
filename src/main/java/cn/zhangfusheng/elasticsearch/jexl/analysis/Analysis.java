package cn.zhangfusheng.elasticsearch.jexl.analysis;

import cn.zhangfusheng.elasticsearch.jexl.Jexl3Execute;

import java.util.Map;

/**
 * jexl3 表达式解析结果
 * @author fusheng.zhang
 * @date 2022-03-05 16:25:37
 */
public interface Analysis {

    void execute(Jexl3Execute jexl3Execute);
}
