package cn.zhangfusheng.elasticsearch.jexl.analysis.impl;

import cn.zhangfusheng.elasticsearch.jexl.Jexl3Execute;
import cn.zhangfusheng.elasticsearch.jexl.Jexl3Util;
import cn.zhangfusheng.elasticsearch.jexl.analysis.Analysis;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlScript;
import org.apache.commons.jexl3.MapContext;

import java.util.Map;

/**
 * join # !
 * @author fusheng.zhang
 * @date 2022-03-07 10:44:12
 */
@Data
@Accessors(chain = true)
public class JoinAnalysis implements Analysis {

    private String joinExp;

    @Override
    public String toString() {
        return joinExp;
    }

    @Override
    public void execute(Jexl3Execute jexl3Execute) {
        Object execute = Jexl3Util.execute(jexl3Execute.getParams(), jexl3Execute.getConstantParams(), Jexl3Util.toStr(joinExp));
        jexl3Execute.setStr(String.valueOf(execute));
    }
}
