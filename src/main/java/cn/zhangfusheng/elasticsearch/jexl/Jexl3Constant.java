package cn.zhangfusheng.elasticsearch.jexl;

import java.util.regex.Pattern;

/**
 * jexl3 常量
 * @author fusheng.zhang
 * @date 2022-03-08 14:22:30
 */
public class Jexl3Constant {

    /**
     * 常量前缀
     */
    public static String CONST_PREFIX = "_mapConsts";
    /**
     * 参数前缀
     */
    public static String VAR_PREFIX = "_mapVars";
    /**
     *
     */
    public static Pattern MAP_PATTERN = Pattern.compile("\\[([.a-zA-Z0-9_]+)]");
    /**
     * 前缀匹配
     */
    public static Pattern PREFIX_PATTERN = Pattern.compile("([:$])([a-zA-Z0-9_]+)(\\.[a-zA-Z0-9_]+)*");

    /**
     * 关键字
     */
    public static final String KEYWORD_$ = "$";
    public static final char KEYWORD_$_ = '$';
    public static final String KEYWORD_SHARP = "#";
    public static final String KEYWORD_JOIN = "!";
    public static final String KEYWORD_IF = "if";
    public static final String KEYWORD_ELSE = "#else";
    public static final String KEYWORD_FOR = "for";

    /**
     * 匹配关键字表达式
     */
    public static final Pattern PATTERN_KEYWORD = Pattern.compile("::|([:$]{1}[a-zA-Z0-9_.]+)|\\{([^{}]+)}\\?|#(#|!|if|for)?");
    /**
     * 匹配 #else 开头
     */
    public static final Pattern PATTERN_ELSE = Pattern.compile("(#else)+");
    /**
     * in 匹配
     */
    public static final Pattern PATTERN_IN = Pattern.compile("([a-zA-Z0-9_]*)\\s+in\\s+(.+)");


}
