package cn.zhangfusheng.elasticsearch.jexl;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 执行 jexl3 解析 转换成 sql
 * @author fusheng.zhang
 * @date 2022-03-08 10:36:33
 */
public class Jexl3Execute {

    @Getter
    @Setter
    private int index;

    @Getter
    private final Map<String, Object> params;

    @Getter
    private final Map<String, Object> constantParams;

    private final List<Object> paramValues;

    private final StringBuilder result;

    public Jexl3Execute(
            Map<String, Object> params, Map<String, Object> constantParams) {
        this.params = CollectionUtils.isEmpty(params) ? new HashMap<>(0) : params;
        this.constantParams = CollectionUtils.isEmpty(constantParams) ? new HashMap<>(0) : constantParams;
        this.paramValues = new ArrayList<>(params.size());
        this.result = new StringBuilder();
    }

    public Jexl3Execute setValue(Object value) {
        this.paramValues.add(value);
        return this;
    }

    public void setStr(String str) {
        this.result.append(str);
    }

    public void setParam(String key, Object value) {
        this.params.put(key, value);
    }

    public String getResult() {
        return result.toString();
    }

    public List<Object> getArgs() {
        return this.paramValues;
    }
}
