package cn.zhangfusheng.elasticsearch.scan;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 11:30:13
 */
public class ElasticSearchRepositoryScanner extends ClassPathBeanDefinitionScanner {

    public ElasticSearchRepositoryScanner(BeanDefinitionRegistry registry) {
        super(registry);
    }

    public ElasticSearchEntityRepositoryFilter build(ElasticSearchEntityRepositoryFilter includeFilter, String... basePackages) {
        super.addIncludeFilter(includeFilter);
        super.doScan(basePackages);
        return includeFilter;
    }
}
