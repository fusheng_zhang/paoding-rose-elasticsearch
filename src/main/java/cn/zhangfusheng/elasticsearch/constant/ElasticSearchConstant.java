package cn.zhangfusheng.elasticsearch.constant;

import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;
import cn.zhangfusheng.elasticsearch.model.analysis.vo.ReturnDetail;
import cn.zhangfusheng.elasticsearch.scan.ElasticSearchEntityRepositoryDetail;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 15:25:32
 */
public final class ElasticSearchConstant {
    /**
     * 空格 换行 制表符
     */
    public static final Pattern PATTERN = Pattern.compile("\\s+|\t|\r|\n");

    public static final String FIELD_PARAM_TYPE = "type";
    public static final String FIELD_PROPERTIES = "properties";

    /**
     * 版本迁移控制的索引名称
     */
    public static final String TRANSFER_INDEX_NAME = "transfer";
    /**
     * 版本号
     */
    public static final int TRANSFER_INDEX_VERSION = 1;
    /**
     * 版本迁移索引
     */
    public static final String TRANSFER_INDEX = String.format("%s_v%s", TRANSFER_INDEX_NAME, TRANSFER_INDEX_VERSION);

    /**
     * 最大查询条数
     */
    public static final Integer MAX_DOC_SIZE = 10000;

    /**
     * 默认分页循环
     */
    public static final Integer DEFAULT_LOOP_PAGE_NUM = 10;

    /**
     * 最大查询条数的四分之一
     */
    public static final Integer THRESHOLD_DOC_SIZE = MAX_DOC_SIZE / DEFAULT_LOOP_PAGE_NUM;

    /**
     * 数据迁移描述
     */
    public static final String DESC_CREATE_INDEX_MAPPING = "创建index和mapping";
    public static final String DESC_UPDATE_MAPPING = "更新mapping";
    public static final String DESC_CREATE_INDEX_MAPPING_AND_TRANSGER_DB = "创建index和mapping,并且迁移数据";

    /**
     * Repository detail cache
     */
    public static final Map<Class<?>, ElasticSearchEntityRepositoryDetail> REPOSITORY_DETAIL_CACHE = new ConcurrentHashMap<>();
    /**
     * 方法要查询的index 的缓存
     */
    public static final Map<Method, String[]> METHOD_INDEX_CACHE = new ConcurrentHashMap<>();
    /**
     * 返回值信息
     */
    public static final Map<Class<?>, ReturnDetail> RETURN_DETAIL_CACHE = new ConcurrentHashMap<>();
    /**
     * 默认采用_id排序
     */
    public static final String SORT_ID = "_id";

    /**
     * JpaSearch 注解的解析缓存
     */
    public static final Map<Method, Map<String, SearchKeyword>> JPA_SEARCH_CACHE = new ConcurrentHashMap<>();

    /**
     * setting config name<br/>
     * 文档对应的settings的唯一标识
     */
    public static final String SETTING_NAME = "name";
    /**
     * settings 的具体配置内容
     */
    public static final String SETTING = "settings";
}
