package cn.zhangfusheng.elasticsearch.constant.enumeration;

/**
 * @author fusheng.zhang
 * @date 2022-06-20 16:35:36
 */
public enum DyanmicType {

    /**
     * 按照sql语句的方式进行解析
     */
    SQL,
    /**
     * 按照 dsl 语句进行解析
     */
    DSL
}
