package cn.zhangfusheng.elasticsearch.constant.enumeration;

/**
 * 迁移数据方式
 * @author fusheng.zhang
 * @date 2022-06-27 14:33:12
 */
public enum TransferType {

    /**
     * 默认迁移
     * 从源index读取全部数据,然后在写入目标索引
     */
    DEFAULT,
    /**
     * 使用 ElasticSearch 的api ReindexRequest 完成数据迁移
     */
    REINDEX
}
