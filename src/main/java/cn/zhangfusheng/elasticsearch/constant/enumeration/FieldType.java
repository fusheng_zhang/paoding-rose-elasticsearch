package cn.zhangfusheng.elasticsearch.constant.enumeration;

/**
 * 字段类型枚举
 * <a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-types.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-types.html</a>
 * @author fusheng.zhang
 * @date 2022-05-02 00:47:51
 */
public enum FieldType {

    Default,
    /*Core datatypes*/
    Text, Keyword,/*string*/
    Long, Integer, Short, Byte, Double, Float, Half_Float, Scaled_Float,/*Numeric*/
    Date,/*Date*/
    Date_Nanos,/*Date nanoseconds*/
    Boolean,/*Boolean*/
    Binary, /*Binary*/
    Integer_Range, Float_Range, Long_Range, Double_Range, Date_Range, /*Range*/

    /*Complex datatypes*/
    Object,/*Object*/
    Nested,/*Nested*/

    /*Geo datatypes*/
    Geo_Point, /*Geo-point*/
    Geo_Shape, /*Geo-shape*/ /*https://www.elastic.co/guide/en/elasticsearch/reference/7.4/geo-shape.html,暂不支持*/

    /*Specialised datatypes*/
    Flattened,
    Alias,
    Arrays,
    Wildcard,
}
