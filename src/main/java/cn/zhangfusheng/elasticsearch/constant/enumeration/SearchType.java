package cn.zhangfusheng.elasticsearch.constant.enumeration;

/**
 * 查询类型
 * @author fusheng.zhang
 * @date 2022-05-16 20:39:23
 */
public enum SearchType {

    /**
     * must and
     */
    MUST,
    /**
     * should or
     */
    SHOULD,
    /**
     * 过滤查询
     */
    FILTER,
}
