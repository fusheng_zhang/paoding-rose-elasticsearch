package cn.zhangfusheng.elasticsearch.constant.enumeration;

import org.elasticsearch.index.query.RangeQueryBuilder;

/**
 * 返回范围查询枚举
 * @author fusheng.zhang
 * @date 2022-04-26 17:34:40
 */
public enum RangeKeyword {

    GT("gt", (v, b) -> b.gt(v)), //大于
    GTE("gte", (v, b) -> b.gte(v)), //大于等于
    LT("lt", (v, b) -> b.lt(v)), //小于
    LTE("lte", (v, b) -> b.lte(v)), //小于等于
    ;
    private final String keyword;
    private final Builder builder;

    RangeKeyword(String keyword, Builder builder) {
        this.keyword = keyword;
        this.builder = builder;
    }

    public String getKeyword() {
        return keyword;
    }

    public void getBuilder(Object value, RangeQueryBuilder builder) {
        this.builder.builder(value, builder);
    }

    @FunctionalInterface
    interface Builder {
        RangeQueryBuilder builder(Object value, RangeQueryBuilder builder);
    }
}
