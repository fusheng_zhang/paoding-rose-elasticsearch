package cn.zhangfusheng.elasticsearch.constant;

import java.util.regex.Pattern;

/**
 * 正则表达式
 */
public final class Regular {
    /**
     * 手机号校验正则表达式
     */
    public final static String PHONE_REGULAR = "^1(3\\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\\d|9[0-35-9])\\d{8}$";
    public final static Pattern PATTERN_PHONE_REGULAR = Pattern.compile(PHONE_REGULAR);

    /**
     * 时间格式正则表达式
     */
    public final static String REGULAR_TIME_YYYY = "^\\d{4}$";
    public final static String REGULAR_TIME_YYYY_MM = "^\\d{4}-\\d{1,2}$";
    public final static String REGULAR_TIME_YYYY_MM_DD = "^\\d{4}-\\d{1,2}-\\d{1,2}$";
    public final static String REGULAR_TIME_UUUU_MM_DD_T_HH_MM_SS = "^\\d{4}-\\d{1,2}-\\d{1,2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}$";
    public final static String REGULAR_TIME_UUUU_MM_DD_T_HH_MM_SS_Z = "^\\d{4}-\\d{1,2}-\\d{1,2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}Z$";

    public final static Pattern PATTERN_TIME_YYYY = Pattern.compile(REGULAR_TIME_YYYY);
    public final static Pattern PATTERN_TIME_YYYY_MM = Pattern.compile(REGULAR_TIME_YYYY_MM);
    public final static Pattern PATTERN_TIME_YYYY_MM_DD = Pattern.compile(REGULAR_TIME_YYYY_MM_DD);
    public final static Pattern PATTERN_TIME_UUUU_MM_DD_T_HH_MM_SS = Pattern.compile(REGULAR_TIME_UUUU_MM_DD_T_HH_MM_SS);
    public final static Pattern PATTERN_TIME_UUUU_MM_DD_T_HH_MM_SS_Z = Pattern.compile(REGULAR_TIME_UUUU_MM_DD_T_HH_MM_SS_Z);

}