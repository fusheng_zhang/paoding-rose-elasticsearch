package cn.zhangfusheng.elasticsearch.function;

import org.elasticsearch.search.sort.SortOrder;

/**
 * @author fusheng.zhang
 * @date 2022-04-29 17:04:37
 */
@FunctionalInterface
public interface DslSortOrderFunction {

    /**
     * 动态的自定义排序规则
     * 返回值为空,则该字段不参与排序
     * @param value 字段值
     * @return
     */
    SortOrder sort(Object value);
}
