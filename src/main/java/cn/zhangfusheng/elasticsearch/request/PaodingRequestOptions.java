package cn.zhangfusheng.elasticsearch.request;

import org.elasticsearch.client.RequestOptions;

/**
 * {@link org.elasticsearch.client.RequestOptions}
 * @author zhangfusheng
 * @date 2024/1/2
 */
public interface PaodingRequestOptions {

    RequestOptions options();

    String requestOptions();

    class DefaultPaodingRequestOptions implements PaodingRequestOptions {
        @Override
        public RequestOptions options() {
            return RequestOptions.DEFAULT;
        }

        @Override
        public String requestOptions() {
            return "default";
        }
    }
}
