package cn.zhangfusheng.elasticsearch.config;

import cn.zhangfusheng.elasticsearch.annotation.EnablePaodingRoseElasticSearch;
import cn.zhangfusheng.elasticsearch.request.PaodingRequestOptions;
import cn.zhangfusheng.elasticsearch.template.ElasticSearchRestTemplate;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.annotation.Role;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-04-20 12:18:35
 */
@ComponentScan("cn.zhangfusheng.elasticsearch")
public class ElasticSearchConfigConfiguration implements ImportAware {

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        AnnotationAttributes.fromMap(
                importMetadata.getAnnotationAttributes(EnablePaodingRoseElasticSearch.class.getName()));
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public PaodingRequestOptions defaultPaodingRequestOptions() {
        return new PaodingRequestOptions.DefaultPaodingRequestOptions();
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public ElasticSearchRestTemplate elasticSearchRestTemplate(RestHighLevelClient restHighLevelClient) {
        return new ElasticSearchRestTemplate(restHighLevelClient);
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public ElasticSearchConfigOperationSourceAdvisor beanFactoryTransactionalOperationSourceAdvisor(
            RestHighLevelClient restHighLevelClient, List<PaodingRequestOptions> paodingRequestOptions) {
        return new ElasticSearchConfigOperationSourceAdvisor(restHighLevelClient, paodingRequestOptions);
    }

}
