package cn.zhangfusheng.elasticsearch.util.date.enumeration;

import java.time.format.DateTimeFormatter;

/**
 * @author fusheng.zhang
 * @Description 时间类型常量
 * @create 2019-08-26 17:11:00
 */
public enum DateFromatEnum {
    /**
     * yyyy-MM-dd HH:mm:ss
     */
    YYYY(DateTimeFormatter.ofPattern(DateFormat.YYYY)),
    YYYY_MM(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM)),
    YYYY_MM_1(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_1)),
    YYYY_MM_2(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_2)),
    YYYY_MM_DD(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD)),
    YYYY_MM_DD_1(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_1)),
    YYYY_MM_DD_2(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_2)),
    YYYY_MM_DD_3(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_3)),
    HH_MM_SS(DateTimeFormatter.ofPattern(DateFormat.HH_MM_SS)),
    HH_MM_SS_1(DateTimeFormatter.ofPattern(DateFormat.HH_MM_SS_1)),
    HH_MM_SS_2(DateTimeFormatter.ofPattern(DateFormat.HH_MM_SS_2)),
    YYYY_MM_DD_HH(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH)),
    YYYY_MM_DD_HH_1(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_1)),
    YYYY_MM_DD_HH_2(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_2)),
    YYYY_MM_DD_HH_MM(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM)),
    YYYY_MM_DD_HH_MM_1(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_1)),
    YYYY_MM_DD_HH_MM_2(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_2)),
    YYYY_MM_DD_HH_MM_SS(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_SS)),
    YYYY_MM_DD_HH_MM_SS_1(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_SS_1)),
    YYYY_MM_DD_HH_MM_SS_2(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_SS_2)),
    YYYY_MM_DD_HH_MM_SS_3(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_SS_3)),
    YYYY_MM_DD_HH_MM_SS_4(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_MM_SS_4)),
    /**************************************************************************/
    UUUU_MM_DD_T_HH_MM_SS(DateTimeFormatter.ofPattern(DateFormat.UUUU_MM_DD_T_HH_MM_SS)),
    UUUU_MM_DD_T_HH_MM_SS_Z(DateTimeFormatter.ofPattern(DateFormat.UUUU_MM_DD_T_HH_MM_SS_Z)),
    UUUU_MM_DD_T_HH_MM_SS_SSS(DateTimeFormatter.ofPattern(DateFormat.UUUU_MM_DD_T_HH_MM_SS_SSS)),
    YYYY_MM_DD_T_HH_MM_SS(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_T_HH_MM_SS)),
    /**************************************************************************/
    YYYY_MM_DD_HH_00_00(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_00_00)),
    YYYY_MM_DD_HH_59_59(DateTimeFormatter.ofPattern(DateFormat.YYYY_MM_DD_HH_59_59)),
    ;


    private transient DateTimeFormatter formatter;

    DateFromatEnum(DateTimeFormatter formatter) {
        this.formatter = formatter;
    }

    public DateTimeFormatter getFormatter() {
        return formatter;
    }

    public void setFormatter(DateTimeFormatter formatter) {
        this.formatter = formatter;
    }

}
