package cn.zhangfusheng.elasticsearch.util.date.enumeration;

/**
 * @author fusheng.zhang
 * @date 2021-08-28 11:06:45
 */
public final class DateFormat {

    public static final String YYYY = "yyyy";
    public static final String YYYY_MM = "yyyy-MM";
    public static final String YYYY_MM_1 = "yyyy/MM";
    public static final String YYYY_MM_2 = "yyyyMM";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MM_DD_1 = "yyyy/MM/dd";
    public static final String YYYY_MM_DD_2 = "yyyyMMdd";
    public static final String YYYY_MM_DD_3 = "yyyy.MM.dd";
    public static final String HH_MM_SS = "HH:mm:ss";
    public static final String HH_MM_SS_1 = "HH/mm/ss";
    public static final String HH_MM_SS_2 = "HHmmss";
    public static final String YYYY_MM_DD_HH = "yyyy-MM-dd HH";
    public static final String YYYY_MM_DD_HH_1 = "yyyy/MM/dd HH";
    public static final String YYYY_MM_DD_HH_2 = "yyyyMMdd HH";
    public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public static final String YYYY_MM_DD_HH_MM_1 = "yyyy/MM/dd HH/mm";
    public static final String YYYY_MM_DD_HH_MM_2 = "yyyyMMdd HHmm";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD_HH_MM_SS_1 = "yyyy/MM/dd HH/mm/ss";
    public static final String YYYY_MM_DD_HH_MM_SS_2 = "yyyyMMdd HHmmss";
    public static final String YYYY_MM_DD_HH_MM_SS_3 = "yyyyMMddHHmmss";
    public static final String YYYY_MM_DD_HH_MM_SS_4 = "yyyy/MM/dd HH:mm:ss";
    /**************************************************************************/
    public static final String UUUU_MM_DD_T_HH_MM_SS = "uuuu-MM-dd'T'HH:mm:ss.SSS";
    public static final String UUUU_MM_DD_T_HH_MM_SS_Z = "uuuu-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String UUUU_MM_DD_T_HH_MM_SS_SSS = "uuuu-MM-dd'T'HH:mm:ss.SSS";
    public static final String YYYY_MM_DD_T_HH_MM_SS = "uuuu-MM-dd'T'HH:mm:ss";
    /**************************************************************************/
    public static final String YYYY_MM_DD_HH_00_00 = "yyyy-MM-dd HH:00:00";
    public static final String YYYY_MM_DD_HH_59_59 = "yyyy-MM-dd HH:59:59";
}
