package cn.zhangfusheng.elasticsearch.util.date.enumeration;

import java.time.temporal.ChronoUnit;

/**
 * @Description
 * @author fusheng.zhang
 * @create 2019-08-29 17:16:00
 */
public enum DateAddEnum {
    /**
     * 年
     */
    YEARS("YEARS"),
    MONTHS("MONTHS"),
    DAYS("DAYS"),
    HOURS("HOURS"),
    MINUTES("MINUTES"),
    SECONDS("SECONDS"),
    WEEKS("WEEKS"),
    ;

    private transient ChronoUnit chronoUnit;

    DateAddEnum(String pattern) {
        chronoUnit = ChronoUnit.valueOf(pattern);
    }

    public ChronoUnit getChronoUnit() {
        return chronoUnit;
    }

    public void setChronoUnit(ChronoUnit chronoUnit) {
        this.chronoUnit = chronoUnit;
    }
}
