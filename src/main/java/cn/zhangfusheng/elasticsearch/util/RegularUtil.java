package cn.zhangfusheng.elasticsearch.util;

import java.util.regex.Pattern;

/**
 * @author fusheng.zhang
 * @date 2021-11-20 15:29:02
 */
public class RegularUtil {

    /**
     * 验证字符串是否符合正则表达式
     * @param str     字符串
     * @param pattern 正则
     * @return
     */
    public static boolean matches(String str, Pattern pattern) {
        return pattern.matcher(str).matches();
    }

}
