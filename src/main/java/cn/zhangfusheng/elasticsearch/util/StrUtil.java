package cn.zhangfusheng.elasticsearch.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.helpers.MessageFormatter;

/**
 * @author zhangfusheng
 * @date 2023/12/23
 */
public class StrUtil {

    /**
     * @param input
     * @return
     */
    public static String camelToSnakeCase(String input) {
        return StringUtils.uncapitalize(StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(input), '_')).toLowerCase();
    }

    /**
     * 类似日志的形式格式化字符产
     * @param messagePattern
     * @param objects
     * @return
     */
    public static String format(String messagePattern, Object... objects) {
        return MessageFormatter.arrayFormat(messagePattern, objects).getMessage();
    }
}
