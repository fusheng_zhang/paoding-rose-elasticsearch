package cn.zhangfusheng.elasticsearch.util;

import cn.zhangfusheng.elasticsearch.util.date.enumeration.DateFormat;
import com.alibaba.fastjson2.JSONFactory;
import com.alibaba.fastjson2.JSONWriter;

/**
 * @author zhangfusheng
 * @date 2024/5/23
 */
public class JsonConstant {

    public static JSONWriter.Context DEFAULT_WRITE_CONTEXT = JSONFactory.createWriteContext();

    static {
        DEFAULT_WRITE_CONTEXT.setDateFormat(DateFormat.YYYY_MM_DD_HH_MM_SS);
        DEFAULT_WRITE_CONTEXT.config(JSONWriter.Feature.WriteMapNullValue, true);
    }
}
