package cn.zhangfusheng.elasticsearch.cycle;

import cn.zhangfusheng.elasticsearch.template.ElasticSearchRestTemplate;

/**
 * 版本控制表的mapping(transfer)创建前执行的函数<br/>
 * 可以创建一些全局的pipeline 等<br/>
 * 该接口函数需要有具体的bean
 * @author fusheng.zhang
 * @date 2022-04-24 11:23:49
 */
@FunctionalInterface
public interface InitTransferBefore {

    void run(ElasticSearchRestTemplate elasticSearchRestTemplate);
}
