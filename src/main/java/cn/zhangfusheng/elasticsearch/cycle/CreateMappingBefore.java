package cn.zhangfusheng.elasticsearch.cycle;

import cn.zhangfusheng.elasticsearch.template.ElasticSearchRestTemplate;

/**
 * 创建 mapping前执行
 * @author fusheng.zhang
 * @date 2022-04-24 11:39:56
 */
@FunctionalInterface
public interface CreateMappingBefore {
    void before(ElasticSearchRestTemplate elasticSearchRestTemplate);
}
