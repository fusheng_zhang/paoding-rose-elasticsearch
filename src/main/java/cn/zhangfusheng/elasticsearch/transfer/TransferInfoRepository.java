package cn.zhangfusheng.elasticsearch.transfer;

import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;

/**
 * 数据迁移
 * @author fusheng.zhang
 * @date 2021-09-02 20:26:56
 */
public interface TransferInfoRepository extends ElasticSearchRepository<TransferInfo> {
}
