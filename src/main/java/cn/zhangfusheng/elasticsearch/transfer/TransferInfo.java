package cn.zhangfusheng.elasticsearch.transfer;

import cn.zhangfusheng.elasticsearch.annotation.document.field.FieldMapping;
import cn.zhangfusheng.elasticsearch.annotation.document.field.MappingParameters;
import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSearch;
import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSortOrder;
import cn.zhangfusheng.elasticsearch.constant.enumeration.FieldType;
import cn.zhangfusheng.elasticsearch.exception.GlobalSystemException;
import cn.zhangfusheng.elasticsearch.util.date.LocalDateTimeUtils;
import cn.zhangfusheng.elasticsearch.util.date.enumeration.DateFormat;
import cn.zhangfusheng.elasticsearch.util.date.enumeration.DateFromatEnum;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * es 数据迁移 版本升级
 * 版本号不变,升级编号变化,更新mapping,不迁移数据
 * 版本号变,升级编号无论是否变化,创建新的index和mapping,并且迁移数据
 * @author fusheng.zhang
 * @date 2022-02-25 10:50:48
 */
@Data
@Accessors(chain = true)
// @IndexDiscription(value = "transfer_info", version = 1, upgradeVersion = "2022-02-25 10:50:48")
public class TransferInfo {

    /**
     * 主键编号
     */
    @FieldMapping(primaryId = true, type = FieldType.Keyword)
    private String rowId;

    /**
     * false: 仅更新mapping
     * true: 升级index,切迁移数据
     */
    private Boolean transfer;

    /**
     * 是否更新别名
     */
    private Boolean updateAlias;

    /**
     * 当前索引名称
     */
    private String indexName;

    /**
     * 上一个索引名称
     */
    private String upIndexName;

    /**
     * 对应的className
     */
    @DslSearch
    @FieldMapping(type = FieldType.Keyword)
    private String className;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 升级编号
     */
    private String upgradeVersion;

    /**
     * 数据迁移编号
     */
    @DslSearch
    @FieldMapping(type = FieldType.Keyword)
    private String transferVersion;

    /**
     * 操作描述
     * createIndexMapping:创建index和mapping,
     * updateMapping:更新mapping
     * createIndexMappingAndTransgerDb:升级index和mapping,并且迁移数据
     */
    private String desc;

    /**
     * 创建时间
     */
    @DslSortOrder(SortOrder.ASC)
    @FieldMapping(mappingParameters = @MappingParameters(format = DateFormat.YYYY_MM_DD_HH_MM_SS))
    private LocalDateTime createTime;

    public TransferInfo(String rowId, Map<String, Object> sourceAsMap) {
        this.rowId = rowId;
        if (sourceAsMap.containsKey("indexName")) indexName = String.valueOf(sourceAsMap.get("indexName"));
        if (sourceAsMap.containsKey("upIndexName")) upIndexName = String.valueOf(sourceAsMap.get("upIndexName"));
        if (sourceAsMap.containsKey("className")) className = String.valueOf(sourceAsMap.get("className"));
        if (sourceAsMap.containsKey("upgradeVersion")) {
            upgradeVersion = String.valueOf(sourceAsMap.get("upgradeVersion"));
        }
        transfer = Boolean.parseBoolean(String.valueOf(sourceAsMap.get("transfer")));
        version = Integer.parseInt(String.valueOf(sourceAsMap.get("version")));
    }

    public TransferInfo() {

    }

    /**
     * 转 XContentBuilder
     * @return
     */
    public XContentBuilder xContentBuilder(String rowId) {
        try {
            XContentBuilder xContentBuilder = XContentFactory.jsonBuilder().startObject();
            xContentBuilder.field("rowId", StringUtils.defaultIfBlank(this.rowId, rowId));
            if (StringUtils.isNotBlank(indexName)) xContentBuilder.field("indexName", indexName);
            if (StringUtils.isNotBlank(upIndexName)) xContentBuilder.field("upIndexName", upIndexName);
            if (StringUtils.isNotBlank(className)) xContentBuilder.field("className", className);
            if (StringUtils.isNotBlank(upgradeVersion)) xContentBuilder.field("upgradeVersion", upgradeVersion);
            if (StringUtils.isNotBlank(transferVersion)) xContentBuilder.field("transferVersion", transferVersion);
            if (StringUtils.isNotBlank(desc)) xContentBuilder.field("desc", desc);
            xContentBuilder.field("transfer", transfer);
            xContentBuilder.field("version", version);
            xContentBuilder.field("createTime", LocalDateTimeUtils.nowTime(DateFromatEnum.YYYY_MM_DD_HH_MM_SS));
            return xContentBuilder.endObject();
        } catch (IOException e) {
            throw new GlobalSystemException(e);
        }
    }
}
