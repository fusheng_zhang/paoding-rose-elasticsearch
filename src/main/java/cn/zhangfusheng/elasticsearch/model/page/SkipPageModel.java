package cn.zhangfusheng.elasticsearch.model.page;

import lombok.Data;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;

import java.util.Objects;

/**
 * 跳页逻辑
 * @author fusheng.zhang
 * @date 2023-02-24 13:33:55
 */
@Data
public class SkipPageModel {

    /**
     * 字段名
     */
    private String fieldName;

    /**
     * 右跳页 大于
     */
    private Object gt;

    /**
     * 左跳页 小于
     */
    private Object lt;

    public RangeQueryBuilder rangeQuery() {
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(fieldName);
        if(Objects.nonNull(gt))rangeQueryBuilder.gt(gt);
        if(Objects.nonNull(lt))rangeQueryBuilder.gt(lt);
        return rangeQueryBuilder;
    }
}
