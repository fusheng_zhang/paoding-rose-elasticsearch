package cn.zhangfusheng.elasticsearch.model.page;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.lucene.search.TotalHits;

import java.util.List;
import java.util.Objects;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 16:13:15
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class PageResponse<T> {

    /**
     * 如果第geo_point 查询,返回的则是距离信息<br/>
     * 分页查询下一页,传递最后一条 search after 即可
     */
    private Object[] searchAfter;

    /**
     * 总条数
     */
    private long total;

    /**
     * 跳过的总条数
     */
    private int skipTotal;

    /**
     * 命中数状态
     */
    private TotalHits.Relation relation;

    private List<T> data;

    public PageResponse(Object[] searchAfter, long total, TotalHits.Relation relation, List<T> data) {
        this.searchAfter = searchAfter;
        this.total = total;
        this.relation = relation;
        this.data = data;
    }

    public PageResponse(Object[] searchAfter, long total, List<T> data) {
        this.searchAfter = searchAfter;
        this.total = total;
        this.data = data;
    }

    public PageResponse(long total, List<T> data) {
        this.total = total;
        this.data = data;
    }

    public PageResponse<T> setTotal(TotalHits totalHits) {
        this.total = Objects.isNull(totalHits) ? 0 : totalHits.value;
        this.relation = totalHits.relation;
        return this;
    }

    public long getTotal() {
        return total + skipTotal;
    }
}
