package cn.zhangfusheng.elasticsearch.model.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

/**
 * 分页参数
 * @author fusheng.zhang
 * @date 2022-04-22 11:24:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageRequest {

    public static final PageRequest DEFAULT = new PageRequest(0, 10);
    public static final PageRequest SEARCH_ONE = new PageRequest(0, 1);

    /**
     * 从1开始
     */
    private Integer from = 1;
    /**
     * 每页的大小
     */
    private Integer size = 10;

    /**
     * PageResponse 响应回的 searchAfter
     */
    private Object[] searchAfter;

    /**
     * 跳页配置
     */
    private List<SkipPageModel> skipPage;

    /**
     * 跳过的总条数<br/>
     */
    private int skipTotal;

    public PageRequest(int from, int size) {
        this.from = from;
        this.size = size;
    }

    public PageRequest(int form, int size, Object[] searchAfter) {
        this.from = form;
        this.size = size;
        this.searchAfter = searchAfter;
    }

    public Integer getFrom() {
        if (Objects.isNull(from)) return null;
        if (from <= 0) return 0;
        return (from - 1) * size;
    }
}
