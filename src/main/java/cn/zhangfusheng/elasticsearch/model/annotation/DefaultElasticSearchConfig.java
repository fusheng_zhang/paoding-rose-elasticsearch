package cn.zhangfusheng.elasticsearch.model.annotation;

import cn.zhangfusheng.elasticsearch.annotation.ElasticSearchConfig;
import org.elasticsearch.action.support.WriteRequest;

import java.lang.annotation.Annotation;

/**
 * ElasticSearchConfig 的默认实现
 * @author fusheng.zhang
 * @date 2022-05-01 16:11:41
 */
@SuppressWarnings("ClassExplicitlyAnnotation")
public class DefaultElasticSearchConfig implements ElasticSearchConfig {

    public static ElasticSearchConfig INSTANCE = new DefaultElasticSearchConfig();

    @Override
    public Class<? extends Annotation> annotationType() {
        return ElasticSearchConfig.class;
    }

    @Override
    public boolean openTransactional() {
        return false;
    }

    @Override
    public Class<? extends Throwable> rollbackFor() {
        return Throwable.class;
    }

    @Override
    public boolean trackTotalHits() {
        // 必须返回true,设计到数据迁移
        return true;
    }

    @Override
    public int scrollSize() {
        return 5000;
    }

    @Override
    public long keepAlive() {
        return 5000L;
    }

    @Override
    public String requestOptions() {
        return "default";
    }

    @Override
    public WriteRequest.RefreshPolicy refreshPolicy() {
        return WriteRequest.RefreshPolicy.NONE;
    }
}
