package cn.zhangfusheng.elasticsearch.model.annotation;

import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSearch;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchType;

import java.lang.annotation.Annotation;

/**
 * @author fusheng.zhang
 * @date 2022-05-17 20:45:42
 */
@SuppressWarnings("ClassExplicitlyAnnotation")
public class DefaultDslSearch implements DslSearch {

    public static DslSearch INSTANCE = new DefaultDslSearch();

    @Override
    public String value() {
        return null;
    }

    @Override
    public boolean routing() {
        return false;
    }

    @Override
    public SearchType searchType() {
        return SearchType.MUST;
    }

    @Override
    public SearchKeyword keyword() {
        return SearchKeyword.TERM;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return DslSearch.class;
    }
}
