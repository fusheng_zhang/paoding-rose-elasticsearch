package cn.zhangfusheng.elasticsearch.model.annotation;

import cn.zhangfusheng.elasticsearch.annotation.document.field.FieldMapping;
import cn.zhangfusheng.elasticsearch.annotation.document.field.MappingParameters;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.Alias;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.GeoPoint;
import cn.zhangfusheng.elasticsearch.constant.enumeration.FieldType;

import java.lang.annotation.Annotation;

/**
 * @author fusheng.zhang
 * @date 2022-05-02 01:00:51
 */
@SuppressWarnings("ClassExplicitlyAnnotation")
public class DefaultFieldMapping implements FieldMapping {

    public static final FieldMapping INSTANCE = new DefaultFieldMapping();

    @Override
    public boolean primaryId() {
        return false;
    }

    @Override
    public boolean routing() {
        return false;
    }

    @Override
    public boolean ignore() {
        return false;
    }

    @Override
    public FieldType type() {
        return FieldType.Default;
    }

    @Override
    public MappingParameters mappingParameters() {
        return null;
    }

    @Override
    public GeoPoint geoPoint() {
        return null;
    }

    @Override
    public Alias alias() {
        return null;
    }

    @Override
    public boolean isCreateTime() {
        return false;
    }

    @Override
    public boolean isUpdateTime() {
        return false;
    }


    @Override
    public Class<? extends Annotation> annotationType() {
        return FieldMapping.class;
    }
}
