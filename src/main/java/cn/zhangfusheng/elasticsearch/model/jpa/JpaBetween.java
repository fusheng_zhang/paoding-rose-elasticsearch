package cn.zhangfusheng.elasticsearch.model.jpa;

/**
 * jpa bwtween 函数
 * @author fusheng.zhang
 * @date 2022-04-15 14:11:24
 */
public class JpaBetween {

    public JpaBetween(Object gt, Object lte) {
        this.gt = gt;
        this.lte = lte;
    }

    public static JpaBetween build(Object gt, Object lte) {
        return new JpaBetween(gt, lte);
    }

    private Object gt;

    private Object lte;

    public Object getGt() {
        return gt;
    }

    public void setGt(Object gt) {
        this.gt = gt;
    }

    public Object getLte() {
        return lte;
    }

    public void setLte(Object lte) {
        this.lte = lte;
    }
}
