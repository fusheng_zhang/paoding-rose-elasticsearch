package cn.zhangfusheng.elasticsearch.model.es;

import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author fusheng.zhang
 * @date 2022-04-23 11:14:34
 */
@Data
@AllArgsConstructor
public class PutPipeline {

    private String id;

    private String description;

    private List<Map<String, Object>> processors;

    public String source() {
        return new JSONObject()
                .fluentPut("description", description)
                .fluentPut("processors", processors)
                .toJSONString();
    }
}
