package cn.zhangfusheng.elasticsearch.model.es;

import lombok.Data;
import org.elasticsearch.common.unit.DistanceUnit;

/**
 * @author fusheng.zhang
 * @date 2022-05-01 21:19:42
 */
@Data
public class GeoDistance {

    /**
     * 指定中心点和查询范围
     * @param lat
     * @param lon
     * @param distance
     */
    public GeoDistance(double lat, double lon, String distance) {
        this.lat = lat;
        this.lon = lon;
        this.distance = distance;
    }

    /**
     * 指定中心点和查询范围
     * @param location
     * @param distance
     */
    public GeoDistance(String location, String distance) {
        String[] latLon = location.split(",");
        this.lat = Double.parseDouble(latLon[0]);
        this.lon = Double.parseDouble(latLon[1]);
        this.distance = distance;
    }

    /**
     * 只指定中心点
     * @param location
     */
    public GeoDistance(String location) {
        String[] latLon = location.split(",");
        this.lat = Double.parseDouble(latLon[0]);
        this.lon = Double.parseDouble(latLon[1]);
    }

    /**
     * 查询中心:纬度
     */
    private double lat;
    /**
     * 查询中心:经度
     */
    private double lon;
    /**
     * 与此查询中心(经纬度)的距离。
     */
    private String distance;
    /**
     * 与查询中心距离的单位<br/>
     * 默认:米
     */
    private DistanceUnit unit = DistanceUnit.METERS;

    /**
     * 地理距离计算方法<br/>
     * 默认ARC
     */
    private org.elasticsearch.common.geo.GeoDistance geoDistance = org.elasticsearch.common.geo.GeoDistance.ARC;
}
