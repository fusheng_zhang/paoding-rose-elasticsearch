package cn.zhangfusheng.elasticsearch.model.analysis.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Field;
import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-05-14 16:25:23
 */
@Data
@AllArgsConstructor
public class ReturnDetail {

    private Class<?> returnType;

    private List<Field> fields;
}
