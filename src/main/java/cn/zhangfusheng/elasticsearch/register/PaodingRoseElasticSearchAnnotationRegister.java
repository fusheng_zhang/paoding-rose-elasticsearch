package cn.zhangfusheng.elasticsearch.register;

import cn.zhangfusheng.elasticsearch.config.ElasticSearchConfigConfiguration;
import org.springframework.context.annotation.AutoProxyRegistrar;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author fusheng.zhang
 * @date 2022-04-20 13:06:00
 */
public class PaodingRoseElasticSearchAnnotationRegister implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{AutoProxyRegistrar.class.getName(), ElasticSearchConfigConfiguration.class.getName()};
    }
}
