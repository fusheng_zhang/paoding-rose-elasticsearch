package cn.zhangfusheng.elasticsearch.template;

import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fusheng.zhang
 * @date 2021-09-10 12:26:32
 */
interface Template {

    Logger log = LoggerFactory.getLogger(Template.class);

    RestHighLevelClient restHighLevelClient();

    BulkProcessor getBulkProcessor();
}
