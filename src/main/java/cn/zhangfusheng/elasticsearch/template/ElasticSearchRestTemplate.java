package cn.zhangfusheng.elasticsearch.template;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * @author fusheng.zhang
 * @date 2022-02-24 13:44:05
 */
@Slf4j
public class ElasticSearchRestTemplate extends AbstractElasticSearchRestTemplate {

    public ElasticSearchRestTemplate(RestHighLevelClient restHighLevelClient) {
        super(restHighLevelClient);
    }

}
