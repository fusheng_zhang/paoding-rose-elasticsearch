package org.apache.commons.jexl;

import cn.zhangfusheng.elasticsearch.jexl.Jexl3Analysis;
import cn.zhangfusheng.elasticsearch.jexl.Jexl3Execute;
import cn.zhangfusheng.elasticsearch.jexl.analysis.Analysis;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.JexlScript;
import org.apache.commons.jexl3.MapContext;
import org.apache.commons.jexl3.internal.Engine;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author fusheng.zhang
 * @date 2022-03-02 15:16:19
 */
@Slf4j
public class JexlTest {

    String sql = "select $expr from tb_user where 1=1 and t=:t" +
            " and id in (:id)" +
            // " #for(key in :m.keySet()){ and `##(:key)`=#(:m[:key]) #if(:key == 'age'){ and key is age } #if(:m[:key] == 'age'){ and age=100 } }" +
            // " #for(key in :m.keySet()){ and `##(:key)`=#(:m[:key]) }" +
            // " #for(key in :m.keySet()){ 1=:name, and 1=2 }" +
            // " #if(:name != null and :age > 0){and name like '%s:name%s' #if(:year>2022){ and year_type = 1}}#else{ and 2= 2 #if(:year>2022){ and year_type = 1}}" +
            " ";
    Jexl3Analysis jexl3Analysis = new Jexl3Analysis(sql);

    @Test
    public void demo() {
        HashMap<String, Object> constantParams = new HashMap<>();
        constantParams.put("desc", "自我介绍");
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", "zhangsan");
        params.put("age", 12);
        List<String> friends = new ArrayList<>();
        friends.add("lisi");
        friends.add("wangwu");
        friends.add("zhaoliu");
        params.put("friends", friends);
        String str = "$desc:#if(:name == 'zhangsan'){我是张三[name=:name]}" +
                " #if(:age == 12){我几年12岁[:age]}" +
                " #if(:friends != null){我有几个朋友:#for(f in :friends){:f,##(:f)}}" +
                " #for(f in :friends){ #if(:f =='lisi'){[##(:f)]这位叫李四}}" +
                " index:#for(f in :friends){,##(:_index)}";
        List<Analysis> analyses = new Jexl3Analysis(str).compile();
        Jexl3Execute jexl3Execute = new Jexl3Execute(params, constantParams);
        analyses.forEach(analysis -> analysis.execute(jexl3Execute));
        System.out.println(jexl3Execute.getResult());
        System.out.println(jexl3Execute.getArgs());
    }

    @Test
    public void execute() {
        Map<String, Object> constantParams = new HashMap<>();
        constantParams.put("expr", "c,d");
        Map<String, Object> params = new HashMap<>();
        Map<String, Object> m = new HashMap<>();
        List<Integer> id = new ArrayList<>();
        id.add(1);
        id.add(2);
        id.add(3);
        params.put("id", id);
        m.put("1", "'1'");
        m.put("name", "dsds");
        m.put("age", "age");
        params.put("m", m);
        params.put("name", "name-");
        params.put("age", 10);
        params.put("year", 2023);
        List<Analysis> analyses = jexl3Analysis.compile();
        Jexl3Execute jexl3Execute = new Jexl3Execute(params, constantParams);
        analyses.forEach(analysis -> analysis.execute(jexl3Execute));
        System.out.println(jexl3Execute.getResult());
        System.out.println(jexl3Execute.getArgs());
        System.out.println(jexl3Execute.getArgs().size());
        System.out.println(jexl3Execute.getResult().split("\\?").length - 1);
    }

    @Test
    public void testStartElse() {
        // Pattern PATTERN_BRACES = Pattern.compile("(?<=\\{)(.*?)(}+)");
        // Pattern PATTERN_BRACES = Pattern.compile("\\{.*}.*?|}.*?/}");
        Pattern PATTERN_BRACES = Pattern.compile("\\{([^}]*)}");
        // Matcher matcher = PATTERN_BRACES.matcher("#for(key in :m.keySet()){ and `##(:key)`=#(:m[:key]) }");
        Matcher matcher = PATTERN_BRACES.matcher(sql);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }


    @Test
    public void test1() {
        JexlEngine engine = new Engine();
        JexlContext context = new MapContext();
        String expressionStr = "array.size()";
        List<Object> array = new ArrayList<Object>();
        array.add("this is an array");
        array.add(2);
        array.add(2);
        context.set("array", array);
        JexlExpression expression = engine.createExpression(expressionStr);
        Object o = expression.evaluate(context);//使用表达式对象开始计算
        System.out.println(o);
    }

    @Test
    public void forTest() {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        JexlEngine jexlEngine = new JexlBuilder().create();
        JexlContext jexlContext = new MapContext();
        jexlContext.set("list", integers);
        JexlScript script = jexlEngine.createScript("for (i  :list){if(i>2){return i}}");
        Object execute = script.execute(jexlContext);
        System.out.println(execute);
        JexlScript script1 = jexlEngine.createScript("list");
        System.out.println(script1.execute(jexlContext));

        for (Integer integer : integers) {
            jexlContext.set("i", integer);
            JexlScript script2 = jexlEngine.createScript("'i='+i");
            System.out.println(script2.execute(jexlContext));
        }
    }

    @Test
    public void testMap() {
        Map<String, Object> params = new HashMap<>();
        Map<String, Object> m = new HashMap<>();
        m.put("1", "1");
        m.put("name", "dsds");
        m.put("age", "dsds");
        params.put("m", m);

        String str = "_mapVars['key']";
        // String str = "_mapVars['m']";
        // String str = "_mapVars['m'][_mapVars['key']]";
        JexlEngine jexlEngine = new JexlBuilder().create();
        JexlContext jexlContext = new MapContext();
        jexlContext.set("_mapVars", params);
        JexlScript script = jexlEngine.createScript(str);
        Object execute = script.execute(jexlContext);
        System.out.println(execute);
    }
}
