package com.paoding.rose.elasticsearch.sql.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author fusheng.zhang
 * @date 2022-03-10 21:02:15
 */
@Data
@Accessors(chain = true)
public class TestModel {
    private int od;
}
