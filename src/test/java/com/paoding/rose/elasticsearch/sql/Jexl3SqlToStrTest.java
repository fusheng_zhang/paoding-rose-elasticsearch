package com.paoding.rose.elasticsearch.sql;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslParams;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.DslWithSql;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.condition.SqlCondition;
import cn.zhangfusheng.elasticsearch.dynamic.DyanmicExecuteResult;
import cn.zhangfusheng.elasticsearch.dynamic.DynamicAnalysis;
import cn.zhangfusheng.elasticsearch.dynamic.SqlAnalysisDetail;
import cn.zhangfusheng.elasticsearch.dynamic.sql.es.ElasticSql2DslParser;
import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;
import com.paoding.rose.elasticsearch.sql.model.TestModel;
import io.github.iamazy.elasticsearch.dsl.sql.model.ElasticSqlParseResult;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author fusheng.zhang
 * @date 2022-03-10 14:40:44
 */
public class Jexl3SqlToStrTest {

    @Test
    public void testUpdate() throws NoSuchMethodException {
        Method updateName = TestDao.class.getMethod("updateName", String.class);
        SqlAnalysisDetail sqlAnalysisDetail = new DynamicAnalysis().analysisWithSql(TestDao.class, updateName);
        DyanmicExecuteResult executeResult = sqlAnalysisDetail.execute(new Object[]{"up_name"});
        String sql = executeResult.getStrResult();
        System.out.println(sql);
        System.out.println(executeResult.getArgs());
        ElasticSqlParseResult parseResult = new ElasticSql2DslParser().parse(sql);
        System.out.println(parseResult.getUpdateByQueryRequest());
    }

    @Test
    public void test() throws NoSuchMethodException {
        Method findById = TestDao.class.getMethod("findById", String.class, Boolean.class, TestModel.class);
        SqlAnalysisDetail sqlAnalysisDetail = new DynamicAnalysis().analysisWithSql(TestDao.class, findById);
        TestModel testMode = new TestModel().setOd(1);
        DyanmicExecuteResult executeResult = sqlAnalysisDetail.execute(new Object[]{"1001", null, testMode});
        String sql = executeResult.getStrResult();
        System.out.println(sql);
        System.out.println(executeResult.getArgs());
        // 替换sql 变量
        int index = 0;
        while (sql.indexOf("?") > 0) {
            sql = sql.replace("?", String.valueOf(executeResult.getArgs().get(index++)));
        }
        System.out.println(sql);
        sql = sql.replaceAll("\\?", "'?'");
        ElasticSqlParseResult parseResult = new ElasticSql2DslParser().parse(sql);
        System.out.println(parseResult.toPrettyDsl(parseResult.getSearchRequest()));
    }

    @Test
    public void testIdIn() throws NoSuchMethodException {
        Method findByIdIn = TestDao.class.getMethod("findByIdIn", List.class);
        List<Integer> ids = Stream.of(1, 2, 3, 4).collect(Collectors.toList());
        SqlAnalysisDetail sqlAnalysisDetail = new DynamicAnalysis().analysisWithSql(TestDao.class, findByIdIn);
        DyanmicExecuteResult execute = sqlAnalysisDetail.execute(new Object[]{ids});
        System.out.println(execute.getStrResult());
        System.out.println(execute.getArgs());

        String sql = execute.getStrResult();

        String[] sqlChars = sql.split("\\?");
        StringBuilder newSql = new StringBuilder();
        for (int i = 0; i < sqlChars.length - 1; i++) {
            Object o = execute.getArgs().get(i);
            if (o instanceof String) {
                o = String.format("'%s'", String.valueOf(o).replace("'", "\\'"));
            }
            newSql.append(sqlChars[i]).append(o);
        }
        System.out.println(newSql);
    }

    interface TestDao extends ElasticSearchRepository<TestModel> {
        String column = "id,name,age";
        String table = "tb_user";

        @DslWithSql(
                value = "select $column from $table where id=:id and id=:id",
                conditions = {
                        @SqlCondition(value = "#if(:id != null){id='9888'}", appendWhere = false),
                        @SqlCondition(value = "#if(:o.od != null){od='9od888'}", appendWhere = false),
                        @SqlCondition(value = "#if(:arg_2){age desc}", splicing = "order by", appendWhere = false)})
        void findById(@DslParams("id") String id, Boolean sort, @DslParams("o") TestModel testMode);

        @DslWithSql("select * from tb_user where id in (:rowIds)")
        void findByIdIn(@DslParams("rowIds") List<String> ids);

        @DslWithSql("update tb_user set name=:name where id=1")
        void updateName(@DslParams("name") String name);
    }
}
