package com.paoding.rose.elasticsearch.demo.repository;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslIndex;
import cn.zhangfusheng.elasticsearch.annotation.dsl.DslParams;
import cn.zhangfusheng.elasticsearch.annotation.dsl.mybatis.DslWithMybatis;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.DslWithSql;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.condition.SqlCondition;
import cn.zhangfusheng.elasticsearch.model.jpa.JpaBetween;
import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;
import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;

import java.util.List;
import java.util.Optional;

/**
 * @author fusheng.zhang
 * @date 2022-02-28 13:39:05
 */
public interface TbCompanyRepository extends ElasticSearchRepository<TbCompany> {

    @DslWithMybatis(id = "findById_1", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany findByIdWithMybatis_1(String rowId);

    @DslWithMybatis(id = "findById0", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany findByIdWithMybatis(@DslParams("rowId") String rowId);

    @DslWithMybatis(id = "findById1", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany findByIdWithMybatis1(@DslParams("rowId") String rowId);

    @DslWithMybatis(id = "findById2", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany findByIdWithMybatis2(@DslParams("rowId") String rowId);

    @DslWithMybatis(id = "findById3", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany findByIdWithMybatis3(@DslParams("p") TbCompany tbCompany);

    @DslWithMybatis(id = "findByIdIn", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    List<TbCompany> findByRowIdInWithMybatis(@DslParams("rowIds") List<String> rowIds);

    // TbCompany findByRowId(String rowId);

    @DslIndex(returnType = TbCompany.class)
    Optional<TbCompany> findByRowId(String rowId);

    List<TbCompany> findByRowIdIn(List<String> rowIds);

    List<TbCompany> findByUpdateTimeBetweenOrderByUpdateTime(JpaBetween jpaBetween);

    @DslWithSql("select * from tb_company where rowId = :rowId")
    TbCompany findByIdWithSql(@DslParams("rowId") String rowId);

    @DslWithSql(value = "select * from tb_company", conditions = {
            @SqlCondition("#if(:rowIds != null){rowId in (:rowIds)}")
    })
    List<TbCompany> findByIdInWithSql(@DslParams("rowIds") List<String> rowId);

    PageResponse<TbCompany> findByUpdateTimeBetweenOrderByUpdateTime(JpaBetween jpaBetween, PageRequest pageRequest);

    @DslWithMybatis(id = "findByIdIn", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    PageResponse<TbCompany> findByRowIdInWithMybatis(@DslParams("rowIds") List<String> rowId, PageRequest pageRequest);

    List<TbCompany> findByRowIdAndAddressNameOrCompanyNameNotIn(String rowId, String aName, List<String> names);
}
