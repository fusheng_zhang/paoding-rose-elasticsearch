package com.paoding.rose.elasticsearch.demo.repository;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslParams;
import cn.zhangfusheng.elasticsearch.annotation.dsl.mybatis.DslWithMybatis;
import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;
import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;

import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-06-08 20:20:01
 */
public interface MybatisCompanyRepository extends ElasticSearchRepository<TbCompany> {

    /**
     * 根据id 查询<br/>
     * 默认参数名验证 ${arg_1}
     * @param rowId
     * @return
     */
    @DslWithMybatis(id = "searchById", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany searchById(String rowId);

    /**
     * 根据id查询<br/>
     * 自定义参数名验证 rowId <br/>
     * 支持 if 标签验证
     * @param rowId
     * @return
     */
    @DslWithMybatis(id = "queryById", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    TbCompany queryById(@DslParams("rowId") String rowId);

    /**
     * rowId in or rowId =
     * @param rowIds
     * @param rowId
     * @return
     */
    @DslWithMybatis(id = "searchByIdInOrId", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    List<TbCompany> searchByIdInOrId(@DslParams("rowIds") List<String> rowIds, @DslParams("rowId") String rowId);

    /**
     * 分页查询创建时间大于等于某个时间的数据
     * @param gteTime
     * @param pageRequest
     * @return
     */
    @DslWithMybatis(id = "searchWithPage", nameSpace = "com.paoding.rose.elasticsearch.demo.model.TbCompany")
    PageResponse<TbCompany> searchWithPage(@DslParams("gteTime") String gteTime, PageRequest pageRequest);
}
