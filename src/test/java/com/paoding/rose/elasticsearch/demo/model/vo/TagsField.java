package com.paoding.rose.elasticsearch.demo.model.vo;

import cn.zhangfusheng.elasticsearch.annotation.document.field.FieldMapping;
import cn.zhangfusheng.elasticsearch.constant.enumeration.FieldType;
import lombok.Data;

/**
 * @author fusheng.zhang
 * @date 2022-05-12 11:43:37
 */
@Data
public class TagsField {

    @FieldMapping(type = FieldType.Keyword)
    private String keyword;
}
