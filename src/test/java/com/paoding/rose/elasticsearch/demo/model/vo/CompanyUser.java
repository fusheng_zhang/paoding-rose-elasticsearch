package com.paoding.rose.elasticsearch.demo.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author fusheng.zhang
 * @date 2022-05-11 19:21:16
 */
@Data
@AllArgsConstructor
public class CompanyUser {

    private String userName;

    private Integer age;
}
