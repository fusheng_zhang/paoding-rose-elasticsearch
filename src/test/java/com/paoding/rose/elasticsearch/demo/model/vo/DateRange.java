package com.paoding.rose.elasticsearch.demo.model.vo;

import cn.zhangfusheng.elasticsearch.annotation.document.field.MappingParameters;
import cn.zhangfusheng.elasticsearch.util.date.enumeration.DateFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author fusheng.zhang
 * @date 2022-05-12 14:22:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DateRange {

    private Date gte;

    @MappingParameters(format = DateFormat.YYYY_MM_DD_HH_MM_SS)
    private LocalDateTime lte;
}
