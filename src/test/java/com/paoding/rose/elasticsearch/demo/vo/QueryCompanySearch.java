package com.paoding.rose.elasticsearch.demo.vo;

import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSearch;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fusheng.zhang
 * @date 2022-05-17 20:50:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryCompanySearch {

    // @DslSearch(searchType = SearchType.SHOULD)
    private String searchName;

    @DslSearch(value = "searchName"/*, searchType = SearchType.SHOULD*/)
    private String searchName2;

    private String companyNameAlias;
}
