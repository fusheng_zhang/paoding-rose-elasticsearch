package com.paoding.rose.elasticsearch.demo;

import cn.zhangfusheng.elasticsearch.annotation.EnablePaodingRoseElasticSearch;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author fusheng.zhang
 * @date 2022-03-02 11:44:13
 */
@SpringBootApplication
@EnablePaodingRoseElasticSearch
public class DemoStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoStartApplication.class, args);
    }
}
