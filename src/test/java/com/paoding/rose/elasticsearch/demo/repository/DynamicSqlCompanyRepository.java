package com.paoding.rose.elasticsearch.demo.repository;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslParams;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.DslWithSql;
import cn.zhangfusheng.elasticsearch.annotation.dsl.sql.condition.SqlCondition;
import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;

import java.util.List;

/**
 * 动态sql查询
 * @author fusheng.zhang
 * @date 2022-06-09 20:19:01
 */
public interface DynamicSqlCompanyRepository extends ElasticSearchRepository<TbCompany> {

    static final String INDEX_NAME = "tb_company";

    @DslWithSql(value = "select * from $INDEX_NAME", conditions = {
            @SqlCondition("#if(:rowIds != null){rowId in (:rowIds)}")
    })
    List<TbCompany> findByIdIn(@DslParams("rowIds") List<String> rowId);

    @DslWithSql(value = "select * from $INDEX_NAME", conditions = {
            @SqlCondition("#for(tbCompany in :tbCompanies){#if(:_index == 1){rowId = :tbCompany.rowId}}")
    })
    List<TbCompany> testFor(@DslParams("tbCompanies") List<TbCompany> tbCompanies);
}
