package com.paoding.rose.elasticsearch.demo.controller;

import cn.zhangfusheng.elasticsearch.annotation.ElasticSearchConfig;
import cn.zhangfusheng.elasticsearch.exception.GlobalSystemException;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.model.vo.CompanyUser;
import com.paoding.rose.elasticsearch.demo.model.vo.DateRange;
import com.paoding.rose.elasticsearch.demo.model.vo.IntegerRangeDetail;
import com.paoding.rose.elasticsearch.demo.repository.TbCompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.support.WriteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * @author fusheng.zhang
 * @date 2022-05-15 08:59:42
 */
@Slf4j
@RestController
@RequestMapping("company")
public class TbCompanyController {

    @Autowired
    private TbCompanyRepository tbCompanyRepository;

    /**
     * 新建一条数据
     * @return
     */
    @GetMapping("save")
    public boolean save() {
        TbCompany tbCompany = new TbCompany()
                .setCreateUserName("fusheng.zhang")
                .setCompanyName("中华第一城").setAddressName("北京市 北京 昌平 北七家")
                .setSearchField("中华城").setSort(1).setGroup(1).setLocation("24.531761,118.150718")
                .setCompanyUsers(Arrays.asList(new CompanyUser("一号用户", 12), new CompanyUser("二号用户", 11)))
                .setCompanyUser(new CompanyUser("创建者", 1))
                .setCompanyUserNested(new CompanyUser("companyUserNested", 0))
                .setTags(Arrays.asList("金", "银", "外卖", "堂食", "自助"))
                .setIntegerRangeDetail(new IntegerRangeDetail(10, 20))
                .setDateRange(new DateRange(new Date(), LocalDateTime.now()))
                .setDefaultTime(LocalDateTime.now()).setDefaultDate(new Date()).setUpdateTime(LocalDateTime.now()).setCreateTime(new Date());
        return tbCompanyRepository.insert(tbCompany);
    }

    @GetMapping("bluckSave")
    @ElasticSearchConfig(openTransactional = true)
    public void bluckSave() {
        for (int i = 0; i < 10010; i++) {
            TbCompany tbCompany = new TbCompany()
                    .setCreateUserName("fusheng.zhang")
                    .setCompanyName("中华第一城_" + i).setAddressName("北京市 北京 昌平 北七家")
                    .setSearchField("中华城").setSort(1).setGroup(1).setLocation("24.531761,118.150718")
                    .setCompanyUsers(Arrays.asList(new CompanyUser("一号用户_" + i, 12), new CompanyUser("二号用户_" + i, 11)))
                    .setCompanyUser(new CompanyUser("创建者_" + i, 1))
                    .setCompanyUserNested(new CompanyUser("companyUserNested", 0))
                    .setTags(Arrays.asList("金", "银", "外卖", "堂食", "自助"))
                    .setIntegerRangeDetail(new IntegerRangeDetail(10, 20))
                    .setDateRange(new DateRange(new Date(), LocalDateTime.now()))
                    .setDefaultTime(LocalDateTime.now()).setDefaultDate(new Date()).setUpdateTime(LocalDateTime.now()).setCreateTime(new Date());
            tbCompanyRepository.insert(tbCompany);
        }
    }

    /**
     * 更新一条数据
     * @param rowId 主键id<br/>
     *              对应的数据不存在,会自动抛出异常: document missing<br/>
     *              数据存在,返回更新结果
     * @return
     */
    @GetMapping("update")
    public boolean update(String rowId) {
        TbCompany tbCompany = new TbCompany()
                .setRowId(rowId).setCreateUserName("fusheng.zhang")
                .setCompanyName("中华第一城").setAddressName("北京市 北京 昌平 北七家")
                .setSearchField("中华城").setSort(1).setGroup(1).setLocation("24.531761,118.150718")
                .setCompanyUsers(Arrays.asList(new CompanyUser("一号用户", 12), new CompanyUser("二号用户", 11)))
                .setTags(Arrays.asList("金", "银", "外卖", "堂食", "自助"))
                .setIntegerRangeDetail(new IntegerRangeDetail(10, 20))
                .setDateRange(new DateRange(new Date(), LocalDateTime.now()))
                .setDefaultTime(LocalDateTime.now()).setDefaultDate(new Date())
                .setUpdateTime(LocalDateTime.now()).setCreateTime(new Date());
        return tbCompanyRepository.updateById(tbCompany, Boolean.TRUE);
    }

    /**
     * 刷新策略测试<br/>
     * 不配置刷新策略,默认为Non.返回值可能为空<br/>
     * 配置刷新策略为WAIT_UNTIL,返回值不会为空<br/>
     * @return
     */
    @GetMapping("refreshPolicy")
    @ElasticSearchConfig(refreshPolicy = WriteRequest.RefreshPolicy.WAIT_UNTIL)
    public Object refreshPolicy() {
        TbCompany tbCompany = new TbCompany()
                .setCreateUserName("fusheng.zhang")
                .setCompanyName("刷新策略").setAddressName("北京市 北京 海淀 上地十街")
                .setSearchField("中华城").setSort(1).setGroup(1).setLocation("24.531761,118.150718")
                .setCompanyUsers(Arrays.asList(new CompanyUser("一号用户", 12), new CompanyUser("二号用户", 11)))
                .setCompanyUser(new CompanyUser("创建者", 1))
                .setCompanyUserNested(new CompanyUser("companyUserNested", 0))
                .setTags(Arrays.asList("金", "银", "外卖", "堂食", "自助"))
                .setIntegerRangeDetail(new IntegerRangeDetail(10, 20))
                .setDateRange(new DateRange(new Date(), LocalDateTime.now()))
                .setDefaultTime(LocalDateTime.now()).setDefaultDate(new Date()).setUpdateTime(LocalDateTime.now()).setCreateTime(new Date());
        boolean insert = tbCompanyRepository.insert(tbCompany);
        log.debug("save success?{}", insert);
        return tbCompanyRepository.findByRowId(tbCompany.getRowId());
    }

    /**
     * 开启事物测试
     * @param rowId1 数据存在
     * @param rowId2 数据不存在.
     * @return
     */
    @GetMapping("openTransactional")
    @ElasticSearchConfig(openTransactional = true)
    public Object openTransactional(String rowId1, String rowId2) {
        boolean d1 = tbCompanyRepository.deleteById(rowId1);
        // rowId不存在,删除返回true
        boolean d2 = tbCompanyRepository.deleteById(rowId2);
        if (d1 || d2) throw new GlobalSystemException("不让删");
        return false;
    }

    /**
     * 根据id查询
     * @param rowId
     * @return
     */
    @GetMapping("findById")
    public Object findById(String rowId) {
        return tbCompanyRepository.findById(rowId);
    }
}
