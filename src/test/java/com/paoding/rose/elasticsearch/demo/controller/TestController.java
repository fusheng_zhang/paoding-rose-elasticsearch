package com.paoding.rose.elasticsearch.demo.controller;

import cn.zhangfusheng.elasticsearch.annotation.ElasticSearchConfig;
import cn.zhangfusheng.elasticsearch.model.es.GeoDistance;
import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.model.vo.CompanyUser;
import com.paoding.rose.elasticsearch.demo.model.vo.DateRange;
import com.paoding.rose.elasticsearch.demo.model.vo.IntegerRangeDetail;
import com.paoding.rose.elasticsearch.demo.repository.TbCompanyRepository;
import com.paoding.rose.elasticsearch.demo.vo.QueryCompanyVo;
import com.paoding.rose.elasticsearch.demo.vo.SearchAllIndexResult;
import com.paoding.rose.elasticsearch.demo.vo.SearhAllIndex;
import org.apache.commons.lang3.RandomUtils;
import org.elasticsearch.action.support.WriteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author fusheng.zhang
 * @date 2022-03-02 11:50:52
 */
@RestController
@RequestMapping
public class TestController {

    @Autowired
    private TbCompanyRepository tbCompanyRepository;

    /**
     * 新建测试
     */
    @GetMapping
    public void insert() {
        TbCompany tbCompany = new TbCompany()
                .setCompanyName("中华第一城").setAddressName("北京 上海 石家庄")
                .setDefaultDate(new Date())
                .setDefaultTime(LocalDateTime.now())
                .setUpdateTime(LocalDateTime.now())
                .setSort(1)
                .setGroup(1)
                .setCompanyUsers(Arrays.asList(new CompanyUser("yi", 1), new CompanyUser("er", 2)))
                .setIntegerRangeDetail(new IntegerRangeDetail(10, 20))
                .setDateRange(new DateRange(new Date(), LocalDateTime.now()))
                .setTags(Arrays.asList("餐饮", "外卖", "堂食"))
                .setLocation("24.531761,118.150718")
                .setCreateTime(new Date());
        boolean insert = tbCompanyRepository.insert(tbCompany);
        System.out.println(insert);
    }

    /**
     * 更新测试
     * @param rowId
     */
    @GetMapping("u")
    public void update(String rowId) {
        TbCompany tbCompany = new TbCompany()
                .setCompanyName("中华第一城").setSearchName("中华 第一 城")
                .setRowId(rowId).setUpdateTime(LocalDateTime.now());
        boolean update = tbCompanyRepository.updateById(tbCompany, Boolean.TRUE);
        System.out.println(update);
    }

    @GetMapping("d")
    public void delete(String rowId) {
        boolean deleteById = tbCompanyRepository.deleteById(rowId, null);
        System.out.println(deleteById);
    }

    @GetMapping("bluck")
    @ElasticSearchConfig(openTransactional = true)
    public void blickRequest() {
        for (int i = 0; i < 20001; i++) {
            TbCompany tbCompany = new TbCompany();
            tbCompany.setCompanyName(String.format("name_%s", i));
            tbCompany.setSearchName(String.format("search %s", i));
            tbCompany.setUpdateTime(LocalDateTime.now());
            tbCompanyRepository.insert(tbCompany);
        }
    }

    @GetMapping("refreshPolicy")
    @ElasticSearchConfig(refreshPolicy = WriteRequest.RefreshPolicy.WAIT_UNTIL)
    public void refreshPolicy() {
        TbCompany tbCompany = new TbCompany();
        tbCompany.setCompanyName(String.format("name_%s", "i"));
        tbCompany.setSearchName(String.format("search %s", "i"));
        tbCompany.setUpdateTime(LocalDateTime.now());
        tbCompanyRepository.insert(tbCompany);
        Optional<TbCompany> company = tbCompanyRepository.findByRowId(tbCompany.getRowId());
        System.out.println(company);
    }

    @GetMapping("findById")
    public void findById(String id) {
        Optional<TbCompany> optional = tbCompanyRepository.findById(id, null);
        optional.ifPresent(System.out::println);
    }

    @GetMapping("findOne")
    public Object findOne(String id) {
        return tbCompanyRepository.findOne(new TbCompany().setRowId(id));
    }

    @GetMapping("findAll")
    @ElasticSearchConfig(trackTotalHits = true)
    public void findAll() {
        TbCompany tbCompany = new TbCompany();
        // .setUpdateTime(LocalDateTimeUtils.parseMatches("2022-04-21 17:35:40"));
        List<TbCompany> all = tbCompanyRepository.findAll(tbCompany);
        System.out.println(all.size());
    }

    @GetMapping("findForPage")
    // @ElasticSearchConfig(trackTotalHits = true)
    public Object findForPage(int form, int size, @RequestParam(required = false) Object[] objects) {
        TbCompany tbCompany = new TbCompany();
        return tbCompanyRepository.findForPage(tbCompany, new PageRequest(form, size, objects));
    }

    @PostMapping("findForPage")
    @ElasticSearchConfig(trackTotalHits = true)
    public Object findForPageAllDb(@RequestBody PageRequest pageRequest) {
        TbCompany tbCompany = new TbCompany();
        return tbCompanyRepository.findForPage(tbCompany, pageRequest);
    }

    @GetMapping("page2")
    public Object findForPage2(int form, int size, @RequestParam(required = false) Object[] objects, String ids, String location) {
        PageRequest pageRequest = new PageRequest(form, size, objects);
        QueryCompanyVo queryCompanyVo = new QueryCompanyVo(Arrays.asList(ids.split(",")), 1, new GeoDistance(location));
        return tbCompanyRepository.findForPage(queryCompanyVo, pageRequest);
    }

    @GetMapping("searchAll")
    public List<SearchAllIndexResult> searchAll() {
        return tbCompanyRepository.findAll(new SearhAllIndex());
    }

    @GetMapping("searchAllWitchPage")
    public PageResponse<SearchAllIndexResult> searchAllWitchPage() {
        return tbCompanyRepository.findForPage(new SearhAllIndex(), new PageRequest());
    }

    @GetMapping("ddddd")
    public void findAllForPage() {
        ArrayList<List<TbCompany>> tbCompanies = new ArrayList<>();
        PageRequest pageRequest = new PageRequest(0, 100);
        PageResponse<TbCompany> tbCompanyPageResponse = tbCompanyRepository.findForPage(new TbCompany(), pageRequest);
        pageRequest.setSearchAfter(tbCompanyPageResponse.getSearchAfter());
        List<TbCompany> data = tbCompanyPageResponse.getData();
        tbCompanies.add(data);
        long page = tbCompanyPageResponse.getTotal() / 100 + 1;
        for (long i = 1; i < page; i++) {
            pageRequest.setFrom((int) i);
            PageResponse<TbCompany> tbCompanyPageResponse_ = tbCompanyRepository.findForPage(new TbCompany(), pageRequest);
            List<TbCompany> data_ = tbCompanyPageResponse_.getData();
            tbCompanies.add(data_);
            pageRequest.setSearchAfter(tbCompanyPageResponse_.getSearchAfter());
        }

        System.out.println();
    }
}
