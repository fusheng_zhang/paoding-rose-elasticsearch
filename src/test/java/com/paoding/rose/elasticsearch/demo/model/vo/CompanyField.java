package com.paoding.rose.elasticsearch.demo.model.vo;

import cn.zhangfusheng.elasticsearch.annotation.document.field.FieldMapping;
import cn.zhangfusheng.elasticsearch.annotation.document.field.MappingParameters;
import lombok.Data;

/**
 * @author fusheng.zhang
 * @date 2022-05-09 20:14:16
 */
@Data
public class CompanyField {

    @FieldMapping(mappingParameters = @MappingParameters(analyzer = "whitespace"))
    private String one;

    @FieldMapping(mappingParameters = @MappingParameters(analyzer = "whitespace"))
    private String two;
}
