package com.paoding.rose.elasticsearch.demo.vo;

import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import lombok.Data;

/**
 * @author fusheng.zhang
 * @date 2022-05-14 15:25:01
 */
@Data
public class SearchAllIndexResult extends TbCompany {

    private String indexName;

    /**
     * 上一个索引名称
     */
    private String upIndexName;

}
