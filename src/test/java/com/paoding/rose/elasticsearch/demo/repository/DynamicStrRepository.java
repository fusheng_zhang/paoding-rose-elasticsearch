package com.paoding.rose.elasticsearch.demo.repository;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslParams;
import cn.zhangfusheng.elasticsearch.annotation.dsl.dynamic.Dynamic;
import cn.zhangfusheng.elasticsearch.annotation.dsl.dynamic.condition.DynamicCondition;
import cn.zhangfusheng.elasticsearch.constant.enumeration.DyanmicType;
import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;

/**
 * @author fusheng.zhang
 * @date 2022-06-20 17:08:45
 */
public interface DynamicStrRepository extends ElasticSearchRepository<TbCompany> {

    @Dynamic(value = "select * from tb_company", type = DyanmicType.SQL, splicing = "where", conditions = {
            @DynamicCondition("#if(:rowId != null){ rowId = :rowId}")
    })
    TbCompany runSql(@DslParams("rowId") String rowId);

    @Dynamic(value = "{\"bool\":{\"must\":[{\"term\":{\"rowId\":{\"value\":\":rowId\"}}}]}}")
    TbCompany runDsl(@DslParams("rowId") String rowId);

    @Dynamic("{'bool':{'must':[" +
            "#if(:rowId != null){{'term':{'rowId':{'value':':rowId'}}}}" +
            "]}}")
    TbCompany runDsl2(@DslParams("rowId") String rowId);

    void runScript(int sort);
}
