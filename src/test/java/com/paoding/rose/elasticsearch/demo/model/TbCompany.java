package com.paoding.rose.elasticsearch.demo.model;

import cn.zhangfusheng.elasticsearch.annotation.document.IndexDescription;
import cn.zhangfusheng.elasticsearch.annotation.document.field.FieldMapping;
import cn.zhangfusheng.elasticsearch.annotation.document.field.MappingParameters;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.Alias;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.GeoPoint;
import cn.zhangfusheng.elasticsearch.annotation.document.field.parameters.IndexPrefixes;
import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSortOrder;
import cn.zhangfusheng.elasticsearch.constant.enumeration.FieldType;
import cn.zhangfusheng.elasticsearch.util.date.enumeration.DateFormat;
import com.paoding.rose.elasticsearch.demo.cycle.CreateCompanyMappingAround;
import com.paoding.rose.elasticsearch.demo.model.vo.CompanyField;
import com.paoding.rose.elasticsearch.demo.model.vo.CompanyUser;
import com.paoding.rose.elasticsearch.demo.model.vo.DateRange;
import com.paoding.rose.elasticsearch.demo.model.vo.IntegerRangeDetail;
import com.paoding.rose.elasticsearch.demo.model.vo.TagsField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-02-28 13:37:11
 */
@Data
@Accessors(chain = true)
@IndexDescription( // index: tbcompany_v1
        value = "tb_company", // 索引名称,默认为全小写的类名
        version = 1, // 索引版本号,
        upgradeVersion = "2024-01-02 20:14:30",// 索引更新(创建.升级.更新)编号
        createMappingBefore = CreateCompanyMappingAround.class, // 创建索引前执行
        createMappingEnd = CreateCompanyMappingAround.class // 创建索引后执行
)
public class TbCompany {

    /**
     * 主键,必须使用primaryId=true,标注字段为主键
     */
    @DslSortOrder
    @FieldMapping(primaryId = true)
    private String rowId;

    /**
     * 利用 patameters 配置文档字段的属性,<br/>
     * 具体查看es文档:<a href="https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-params.html">https://www.elastic.co/guide/en/elasticsearch/reference/7.4/mapping-params.html </a>
     */
    @FieldMapping(mappingParameters = @MappingParameters(index_prefixes = @IndexPrefixes))
    private String createUserName;

    /**
     * analyzer.copy_to.fields 的 Mapping Patameters的使用
     */
    @FieldMapping(mappingParameters = @MappingParameters(analyzer = "keyword", copy_to = {"searchName", "searchField"}, fields = CompanyField.class))
    private String companyName;

    @FieldMapping(mappingParameters = @MappingParameters(copy_to = {"searchName", "searchField"}))
    private String addressName;

    @FieldMapping(mappingParameters = @MappingParameters(analyzer = "whitespace"))
    private String searchName;

    @FieldMapping(mappingParameters = @MappingParameters(analyzer = "whitespace"))
    private String searchField;

    private Integer sort;

    @FieldMapping(routing = true)
    private Integer group;

    /**
     * 地理 geo_point 字段的配置
     */
    @FieldMapping(type = FieldType.Geo_Point, geoPoint = @GeoPoint)
    private String location;

    /**
     * arrays 类型字段的配置使用
     */
    @FieldMapping(type = FieldType.Arrays, mappingParameters = @MappingParameters(properties = CompanyUser.class))
    private List<CompanyUser> companyUsers;

    /**
     * object 类型字段的使用
     */
    @FieldMapping(type = FieldType.Object, mappingParameters = @MappingParameters(properties = CompanyUser.class))
    private CompanyUser companyUser;

    /**
     * Nested 字段类型
     */
    @FieldMapping(type = FieldType.Nested)
    private CompanyUser companyUserNested;

    /**
     * Alias 字段类型
     */
    @FieldMapping(type = FieldType.Alias, alias = @Alias(path = "companyUser.userName"))
    private String companyUserNameAlias;

    @FieldMapping(mappingParameters = @MappingParameters(fields = TagsField.class), type = FieldType.Text)
    private List<String> tags;

    /**
     * range 字段类型
     */
    @FieldMapping(type = FieldType.Integer_Range)
    private IntegerRangeDetail integerRangeDetail;

    /**
     * date_range 字段类型
     */
    @FieldMapping(type = FieldType.Date_Range,
            mappingParameters = @MappingParameters(format = "uuuu-MM-dd'T'HH:mm:ss.SSS'Z'||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"))
    private DateRange dateRange;

    private LocalDateTime defaultTime;

    private Date defaultDate;

    /**
     * 时间格式化
     */
    @FieldMapping(mappingParameters = @MappingParameters(format = DateFormat.YYYY_MM_DD_HH_MM_SS))
    private LocalDateTime updateTime;

    @FieldMapping(mappingParameters = @MappingParameters(format = DateFormat.YYYY_MM_DD_HH_MM_SS))
    private Date createTime;
}
