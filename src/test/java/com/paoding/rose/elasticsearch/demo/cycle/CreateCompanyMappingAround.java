package com.paoding.rose.elasticsearch.demo.cycle;

import cn.zhangfusheng.elasticsearch.cycle.CreateMappingBefore;
import cn.zhangfusheng.elasticsearch.cycle.CreateMappingEnd;
import cn.zhangfusheng.elasticsearch.template.ElasticSearchRestTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * @author fusheng.zhang
 * @date 2022-04-24 11:49:34
 */
@Slf4j
public class CreateCompanyMappingAround implements CreateMappingBefore, CreateMappingEnd {
    @Override
    public void before(ElasticSearchRestTemplate elasticSearchRestTemplate) {
        log.info("创建 company 前执行");
    }

    @Override
    public void end(ElasticSearchRestTemplate elasticSearchRestTemplate) {
        log.info("创建 company mapping 后执行");
    }
}
