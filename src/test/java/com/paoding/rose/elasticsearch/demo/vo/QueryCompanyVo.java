package com.paoding.rose.elasticsearch.demo.vo;

import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSearch;
import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSortOrder;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;
import cn.zhangfusheng.elasticsearch.model.es.GeoDistance;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-04-26 20:32:40
 */
@Data
@AllArgsConstructor
public class QueryCompanyVo {

    @DslSearch(value = "rowId", keyword = SearchKeyword.TERMS)
    private List<String> rowIds;

    @DslSearch(routing = true)
    private Integer group;

    @DslSortOrder(fieldName = "location")
    @DslSearch(keyword = SearchKeyword.GEO_DISTANCE)
    private GeoDistance geoDistance;
}
