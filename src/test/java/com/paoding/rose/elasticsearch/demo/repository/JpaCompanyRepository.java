package com.paoding.rose.elasticsearch.demo.repository;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslIndex;
import cn.zhangfusheng.elasticsearch.annotation.dsl.jpa.JpaSearch;
import cn.zhangfusheng.elasticsearch.annotation.dsl.jpa.JpaSearchs;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;
import cn.zhangfusheng.elasticsearch.model.jpa.JpaBetween;
import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;
import cn.zhangfusheng.elasticsearch.repository.ElasticSearchRepository;
import cn.zhangfusheng.elasticsearch.transfer.TransferInfo;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.vo.SearchAllIndexResult;

import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-05-26 14:57:38
 */
public interface JpaCompanyRepository extends ElasticSearchRepository<TbCompany> {

    /**
     * 根据rowId 查询
     * @param rowId
     * @return
     */
    TbCompany findByRowId(String rowId);

    /**
     * 自定查询关键字
     * @param companyName
     * @return
     */
    @JpaSearchs({@JpaSearch(value = "companyName", searchKeyword = SearchKeyword.MATCH_PHRASE)})
    List<TbCompany> findByCompanyNameLike(String companyName);

    /**
     * 范围查询
     * @param jpaBetween
     * @return
     */
    List<TbCompany> findByCreateTimeBetween(JpaBetween jpaBetween);

    /**
     * 自定义查询关键字,以及分页查询
     * @param companyName
     * @param pageRequest
     * @return
     */
    @JpaSearch(value = "companyName", searchKeyword = SearchKeyword.WILDCARD)
    PageResponse<TbCompany> findByCompanyNameLike(String companyName, PageRequest pageRequest);


    @JpaSearch(value = "searchName", searchKeyword = SearchKeyword.WILDCARD)
    @DslIndex(value = {TbCompany.class, TransferInfo.class})
    List<SearchAllIndexResult> findBySearchNameLike(String searchName);
}
