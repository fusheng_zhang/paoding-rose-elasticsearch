package com.paoding.rose.elasticsearch.demo.vo;

import cn.zhangfusheng.elasticsearch.annotation.dsl.DslIndex;
import cn.zhangfusheng.elasticsearch.transfer.TransferInfo;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import lombok.Data;

/**
 * @author fusheng.zhang
 * @date 2022-05-14 15:23:02
 */
@Data
@DslIndex(value = {TbCompany.class, TransferInfo.class}, returnType = SearchAllIndexResult.class)
public class SearhAllIndex extends TbCompany {

    private String indexName;

    /**
     * 上一个索引名称
     */
    private String upIndexName;

}
