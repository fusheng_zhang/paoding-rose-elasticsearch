package com.paoding.rose.elasticsearch.demo.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author fusheng.zhang
 * @date 2022-05-12 14:04:01
 */
@Data
@AllArgsConstructor
public class IntegerRangeDetail {

    private Integer gte;

    private Integer lte;
}
