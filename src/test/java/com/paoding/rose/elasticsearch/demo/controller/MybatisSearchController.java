package com.paoding.rose.elasticsearch.demo.controller;

import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.repository.MybatisCompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * mybatis 用户查询
 * @author fusheng.zhang
 * @date 2022-06-08 20:20:26
 */
@Slf4j
@RestController
@RequestMapping("search/mybatis/company")
public class MybatisSearchController {

    @Autowired
    private MybatisCompanyRepository mybatisCompanyRepository;

    @GetMapping("searchById")
    public Object searchById(String rowId) {
        return mybatisCompanyRepository.searchById(rowId);
    }

    @GetMapping("queryById")
    public Object queryById(String rowId) {
        return mybatisCompanyRepository.queryById(rowId);
    }

    @GetMapping("searchByIdInOrId")
    public Object searchByIdInOrId(String rowId, String rowIds) {
        return mybatisCompanyRepository.searchByIdInOrId(Arrays.asList(rowIds.split(",")), rowId);
    }

    @GetMapping("searchWithPage")
    public PageResponse<TbCompany> searchWithPage(String gteTime, PageRequest pageRequest) {
        return mybatisCompanyRepository.searchWithPage(gteTime, pageRequest);
    }
}
