package com.paoding.rose.elasticsearch.demo.controller;

import cn.zhangfusheng.elasticsearch.annotation.ElasticSearchConfig;
import cn.zhangfusheng.elasticsearch.model.jpa.JpaBetween;
import cn.zhangfusheng.elasticsearch.model.page.PageRequest;
import cn.zhangfusheng.elasticsearch.model.page.PageResponse;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.repository.JpaCompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 基本方法查询查询
 * @author fusheng.zhang
 * @date 2022-05-26 14:56:13
 */
@Slf4j
@RestController
@RequestMapping("search/jpa/company")
public class JpaSearchCompanyController {

    @Autowired
    private JpaCompanyRepository jpaCompanyRepository;

    /**
     * index:[tbcompany_v1],routing:null,
     * queryJson:{"size":10000,"query":{"bool":{"must":[{"term":{"rowId":{"value":"6xYXcnYxTO-eHaJ3gaNGoA","boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}}
     * @param rowId
     * @return
     */
    @GetMapping("findByRowId")
    public Object findByRowId(String rowId) {
        return jpaCompanyRepository.findByRowId(rowId);
    }

    /**
     * 模糊查询以及自定义查询关键字
     * index:[tbcompany_v1],routing:null,
     * 不使用JpaSearch 注解:queryJson:{"size":10000,"query":{"bool":{"must":[{"match":{"companyName":{"query":"中华第一城","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}}
     * 使用JpaSearch 注解:queryJson:{"size":10000,"query":{"bool":{"must":[{"match_phrase":{"companyName":{"query":"中华第一城","slop":0,"zero_terms_query":"NONE","boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}}
     * @param companyName
     * @return
     */
    @GetMapping("findByCompanyNameLike")
    public Object findByCompanyNameLike(String companyName) {
        return jpaCompanyRepository.findByCompanyNameLike(companyName);
    }

    /**
     * between and 查询
     * index:[tbcompany_v1],routing:null,
     * queryJson:{"size":10000,"query":{"bool":{"must":[{"range":{"createTime":{"from":"2022-05-10 00:00:00","to":"2022-05-21 23:59:59","include_lower":false,"include_upper":true,"boost":1.0}}}],"adjust_pure_negative":true,"boost":1.0}}}
     * @return
     */
    @GetMapping("findByCreateTimeBetween")
    public Object findByCreateTimeBetween() {
        JpaBetween jpaBetween = JpaBetween.build("2022-04-10 00:00:00", "2022-06-21 23:59:59");
        return jpaCompanyRepository.findByCreateTimeBetween(jpaBetween);
    }

    /**
     * 分页查询<br/>
     * 无条件的分页查询建议使用Repository.findForPage<br/>
     * @param pageRequest
     * @return
     */
    @GetMapping("findForPage")
    @ElasticSearchConfig(trackTotalHits = true)
    public PageResponse<TbCompany> findForPage(PageRequest pageRequest) {
        return jpaCompanyRepository.findByCompanyNameLike("*中华第一城*", pageRequest);
    }

    /**
     * 查询多个索引测试
     * @param searchName
     * @return
     */
    @GetMapping("findBySearchNameLike")
    public Object findBySearchNameLike(String searchName) {
        return jpaCompanyRepository.findBySearchNameLike(searchName);
    }
}
