package com.paoding.rose.elasticsearch.demo.vo;

import cn.zhangfusheng.elasticsearch.annotation.dsl.es.DslSearch;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchKeyword;
import cn.zhangfusheng.elasticsearch.constant.enumeration.SearchType;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fusheng.zhang
 * @date 2022-05-15 09:12:19
 */
@Data
@NoArgsConstructor
public class QueryCompanyOneVo {

    @DslSearch(keyword = SearchKeyword.WILDCARD)
    private String companyName;

    @DslSearch(value = "companyUser.userName", keyword = SearchKeyword.MATCH)
    private String createUserName;

    @DslSearch(searchType = SearchType.SHOULD)
    private QueryCompanySearch queryCompanySearch;

    public QueryCompanyOneVo(String companyName, String createUserName, QueryCompanySearch queryCompanySearch) {
        this.companyName = companyName;
        this.createUserName = createUserName;
        this.queryCompanySearch = queryCompanySearch;
    }
}
