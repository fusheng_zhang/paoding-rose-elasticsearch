package com.paoding.rose.elasticsearch.demo.controller;

import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.repository.DynamicSqlCompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2022-06-09 20:19:30
 */
@Slf4j
@RestController
@RequestMapping("search/dynamic/company")
public class DynamicSqlSearchController {

    @Autowired
    private DynamicSqlCompanyRepository dynamicSqlCompanyRepository;

    @GetMapping("findByIdIn")
    public Object findByIdIn(String rowIds) {
        return dynamicSqlCompanyRepository.findByIdIn(Arrays.asList(rowIds.split(",")));
    }

    @GetMapping("testFor")
    public Object testFor() {
        List<TbCompany> tbCompanies = Arrays.asList(new TbCompany(), new TbCompany());
        return dynamicSqlCompanyRepository.testFor(tbCompanies);
    }
}
