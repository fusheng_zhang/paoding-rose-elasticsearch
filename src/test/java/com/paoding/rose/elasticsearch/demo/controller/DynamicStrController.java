package com.paoding.rose.elasticsearch.demo.controller;

import com.paoding.rose.elasticsearch.demo.repository.DynamicStrRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fusheng.zhang
 * @date 2022-06-20 17:09:20
 */
@Slf4j
@RestController
@RequestMapping("dynamic/str")
public class DynamicStrController {

    @Autowired
    private DynamicStrRepository dynamicStrRepository;

    @GetMapping("findByRowId")
    public Object runDsl(String rowId) {
        return dynamicStrRepository.runDsl2(rowId);
    }
}
