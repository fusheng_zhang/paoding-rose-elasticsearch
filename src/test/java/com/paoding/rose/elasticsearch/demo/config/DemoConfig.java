package com.paoding.rose.elasticsearch.demo.config;

import cn.zhangfusheng.elasticsearch.cycle.InitTransferBefore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author fusheng.zhang
 * @date 2022-03-02 11:57:28
 */
@Slf4j
@Configuration
public class DemoConfig {

    @Bean
    public InitTransferBefore initTransferBefore() {
        return elasticSearchRestTemplate -> {
            log.info("初始化 数据迁移表 transfer 前执行");
        };
    }
}
