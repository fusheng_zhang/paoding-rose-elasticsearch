package com.paoding.rose.elasticsearch.jpa;

import cn.zhangfusheng.elasticsearch.template.TemplateJpaApi;
import com.paoding.rose.elasticsearch.demo.model.TbCompany;
import com.paoding.rose.elasticsearch.demo.repository.TbCompanyRepository;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * @author fusheng.zhang
 * @date 2025/2/20
 */
public class TestTemplateJpaApi {

    TemplateJpaApi templateJpaApi = new TemplateJpaApi() {
        @Override
        public RestHighLevelClient restHighLevelClient() {
            return null;
        }

        @Override
        public BulkProcessor getBulkProcessor() {
            return null;
        }
    };

    @Test
    public void test() throws Exception {
        Method method = TbCompanyRepository.class.getDeclaredMethod(
                "findByRowIdAndAddressNameOrCompanyNameNotIn", String.class, String.class, List.class);
        List<Object> args = Arrays.asList("3030", "2020", Arrays.asList("1", "2"));
        BoolQueryBuilder boolQueryBuilder = templateJpaApi.buildBoolQueryWithJap(method, null, args, TbCompany.class);
        System.out.println(boolQueryBuilder);
    }


}
